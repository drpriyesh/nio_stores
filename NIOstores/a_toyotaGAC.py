import requests
import json
import pandas as pd
from province_city_pop import pcpop
import time
import cookiejar
import sys
import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
#
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
# path2 = sys.argv[1]
from pathfinder import pathfinder, picklepath

company = 'Toyota_GAC'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')


def toyotagac(path):

    headers = {
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Referer': 'https://www.gac-toyota.com.cn/buy/shopping/dealer-search',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    }
    cookies = cookiejar.toyotaGAC()['cookies']
    headers = cookiejar.toyotaGAC()['headers']
    url1 = cookiejar.toyotaGAC()['url1']
    url2 = cookiejar.toyotaGAC()['url2']
    get_city_codes = requests.get(url1, headers=headers)
    print(get_city_codes.json())
    cities_dict = {}
    cities = []
    for c in get_city_codes.json()['address']:
        cities_dict[str(c['Code'])] = str(c['Name'])
        cities.append(c)


    response = requests.get(url2)

    a = response.text[16:]
    b = json.loads(a)
    r = []
    r2 = []

    count = 0
    for dealer in b:


        datadict = {}
        datadict2 = {}
        citycode = dealer['City']
        print(citycode)
        try:
            city = cities_dict[str(citycode)[1:len(str(citycode))]]
        except KeyError:
            city = ''

        print(city)
        # count += 1
        # print(count, dealer['Address'])

        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = citycode
            normal['province'] = citycode
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = citycode
            normal['province'] = citycode
            normal['tier'] = '其他'
        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']

        datadict['1_dealerID'] = dealer['dealerid']
        datadict['2_name'] = dealer['DealerName']
        datadict['3_type_0'] = 'sales and service'
        datadict['3_type_1'] = ''
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        datadict['6_address'] = dealer['Address']
        datadict['7_hours'] = ''
        datadict['8_tel_sales'] = dealer['Tel']
        datadict['8_tel_service'] = dealer['As_Tel']
        r2.append(datadict)
        datadict2 = {**datadict, **dealer}
        r.append(datadict2)
    # df = pd.DataFrame(r)
    # df.to_excel(path, sheet_name=company)
    # print("Scraping complete: ", path)

    df = pd.DataFrame(r)
    df2 = pd.DataFrame(cities)
    df3 = pd.DataFrame(r2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df3.astype(str).drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df3.to_excel(writer, sheet_name=company + '_1')
        df2.to_excel(writer, sheet_name="city_ids")

    print("Scraping complete: ", path)


# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    toyotagac(path)


# {'DealerIndex': '07', 'City': '065001010', 'dealerid': '0A07A4D9-697B-482E-A090-38681157D41D', 'DealerCode': '33C50', 'DealerName': '广汽丰田温州天信ealerURL': 'https://www.gac-toyota.com.cn/province/zhejiang/wenzhou/dealer/wztxlx', 'ScoreTotal': '0.96', 'Tel': '0577-59925026', 'As_Tel': '0577-59926666', 'Address': '温州市灵溪镇通福路3488号(苍南高速路口右侧)', 'Longitude': '120.441413', 'Latitude': '27.541403'}, {'DealerIndex': '01', 'City': '065001011', 'dealerid': '13EE6E2D-6B69-4169-84EC-FA84BBA7B287', 'DealerCode': '33L10', 'DealerName': '广汽丰田舟山龙华定海店', 'DealerURL': 'https://www.gac-toyota.com.cn/province/zhejiang/zhoushan/dealer/zslhdh', 'ScoreTotal':el': '0580-8668999', 'As_Tel': '0580-8668999', 'Address': '浙江省舟山市定海区双桥镇石礁社区外山头88号（舟山汽车城）', 'Longitude': '122.03388', 'Latitude': '30.028637'}]
