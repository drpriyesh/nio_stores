import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NIOstores.settings')
django.setup()

from nio_stores.models import dataYear, dataMonth, Province, City, Brand, StoreAddress, Store, yearlySalesbycity,monthlySalesbycity,Totals
from province_city_pop import pcpop
from add_functions import add_dataYear, add_dataMonth, add_province, add_city, add_brand, add_storeaddress, add_store, add_ysbc, add_msbc, add_totals, add_orphantotals
import pandas as pd


salespath = '/Users/NIO/NIOstores/NIOstores/data/202107/sales/2016-2021.1开票+上险_2021_07_0819.xlsx'

brands = ['理想','小鹏']
# 'TESLA','NIO','理想','小鹏','VW','TOYOTA'
# brand = 'TESLA'
month = input('Enter month of data in number format (1 to 12):') or '7'
year = input('Enter year of data in 4 digit format (20XX):') or '2021'
for brand in brands:
    df = pd.read_excel(salespath, sheet_name=brand, skiprows=12)
    df = df.drop(['Unnamed: 0'], 1)
    df2 = df
    b = Brand.objects.get_or_create(company_cn=brand)[0]
    m = dataMonth.objects.get_or_create(month=month)[0]
    y = add_dataYear(year=year)

    csq = len(list(Store.objects.filter(brand=b, month=m).values()))+1
    city_list = list(City.objects.filter().values('city'))

    city_list2=[]
    for line in city_list:
        # print(line['city'])
        city_list2.append(line['city'])
    # print('city list', city_list2)
    df_sales = df.to_dict('records')
    # print(df_sales[-1])
    unknown = []
    known = []
    for row in df_sales[0:-1]:
        print(brand, 'a: ', row, row['市'][-1])
        if row['市'][-1] == '市':

            city = row['市'][0:-1]
            print(brand, 'b: ', '市', city)
        else:
            city = row['市']
            print(brand, 'c: ', 'no市', city)

        if city in city_list2:
            # print(city)
            normal = pcpop(city)
            # try:
            #     normal = pcpop(city)
            # except TypeError:
            #     normal['city'] = city
            #     normal['province'] = city
            #     normal['tier'] = '其他'
            # if not normal:
            #     normal['city'] = city
            #     normal['province'] = city
            #     normal['tier'] = '其他'
            c_norm = normal['city']
            p_norm = normal['province']
            t_norm = normal['tier']
            # print(c_norm, p_norm, t_norm)


            c = add_city(c_norm, p_norm, t_norm)

            p = add_province(province=c.province)
            pq = int(row['Grand Total'])
            cpq = int(df['Grand Total'][len(df)-1])
            # print(p, pq, cpq)
            t = add_totals(b, p, c, y, m, pq, cpq, csq)
            known.append(city)
        else:
            print(brand, 'Unknown:', city)
            unknown.append(city)

    count = 1
    # print(unknown)
    for line in unknown:
        print(brand, 'd: ', count, 'unknown: ',line)
        count+=1
    count = 1
    for line in known:
        print(brand, 'e: ', count, 'known: ',line)
        count += 1

    rename ={'阿克苏地区':['新疆维吾尔自治区','阿克苏',''],
    '阿勒泰地区':['新疆维吾尔自治区','阿勒泰地区','orphan'],
    '昌吉回族自治州':['新疆维吾尔自治区','昌吉',''],
    '楚雄彝族自治州':['云南','楚雄',''],
    '大理白族自治州':['云南','大理',''],
    '恩施土家族苗族自治州':['湖北','恩施',''],
    '固原':['宁夏回族自治区','固原','orphan'],
    '海南藏族自治州':['青海','海南藏族自治州','orphan'],
    '和田地区':['新疆维吾尔自治区','和田',''],
    '黄南藏族自治州':['青海','黄南藏族自治州','orphan'],
    '喀什地区':['新疆维吾尔自治区','喀什',''],
    '临夏回族自治州':['甘肃','临夏',''],
    '怒江傈僳族自治州':['云南','怒江傈僳族自治州','orphan'],
    '山南':['西藏自治区','山南','orphan'],
    '省直辖县级行政区划_海南':['海南','省直辖县级行政区划_海南','orphan'],
    '省直辖县级行政区划_河南':['河南','省直辖县级行政区划_河南','orphan'],
    '省直辖县级行政区划_湖北':['湖北','省直辖县级行政区划_湖北','orphan'],
    '文山壮族苗族自治州':['云南','文山',''],
    '自治区直辖县级行政区划':['自治区直辖县级行政区划','自治区直辖县级行政区划','orphan'],
    '阿坝藏族羌族自治州':['四川','阿坝藏族羌族自治州','orphan'],
    '迪庆藏族自治州':['云南','迪庆藏族自治州','orphan'],
    # '': ['', '', 'orphan'],
    '三沙': ['海南', '三沙', 'orphan'],
    '塔城地区': ['新疆维吾尔自治区', '塔城地区', 'orphan'],
    '吐鲁番': ['新疆维吾尔自治区','吐鲁番', 'orphan'],
             '阿拉尔': ['新疆维吾尔自治区', '阿拉尔', 'orphan'],
             '阿里地区': ['西藏自治区', '阿里地区', 'orphan'],
             '北屯': ['新疆维吾尔自治区', '北屯', 'orphan'],
             '昌都': ['西藏自治区', '昌都', 'orphan'],
             '甘南藏族自治州': ['甘肃', '甘南藏族自治州', 'orphan'],
             '甘孜藏族自治州': ['甘肃', '甘孜藏族自治州', 'orphan'],
             '果洛藏族自治州': ['青海', '果洛藏族自治州', 'orphan'],
             '海北藏族自治州': ['青海', '海北藏族自治州', 'orphan'],
             '和田地区': ['新疆维吾尔自治区', '和田地区', 'orphan'],
             '克孜勒苏柯尔克孜自治州': ['新疆维吾尔自治区', '克孜勒苏柯尔克孜自治州', 'orphan'],
             '莱芜': ['山东', '莱芜', 'orphan'],
             '那曲地区': ['西藏自治区', '那曲地区', 'orphan'],
             '神农架林区': ['湖北', '神农架林区', 'orphan'],
             '双河': ['新疆维吾尔自治区', '双河', 'orphan'],
             '图木舒克': ['新疆维吾尔自治区', '图木舒克', 'orphan'],
             '五家渠': ['新疆维吾尔自治区', '五家渠', 'orphan'],
             '玉树藏族自治州': ['青海', '玉树藏族自治州', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],
             # '': ['', '', 'orphan'],


    # '': ['', '', 'orphan'],
    # '海西蒙古族藏族自治州':['青海','海西蒙古族藏族自治州','orphan'],

             }



    for city in unknown:
        real_city = rename[city][1]
        for row in df_sales[0:-1]:
            print(brand, 'f: ', ', row: ', row['市'], ', real_city: ', real_city)
            if row['市'][-1] == '市':
                if row['市'][0:-1] in real_city:
                    print('g: ', count, row)
                    break
            else:
                if row['市'] in real_city:
                    print(brand, 'g: ', count, row)
                    break

        n = pcpop(real_city)
        print(brand, 'h: ', count, n)
        province = n['province']
        ncity = n['city']
        tier = n['tier']
        c = add_city(ncity, province, tier)
        p = add_province(province=c.province)
        pq = int(row['Grand Total'])
        cpq = int(df['Grand Total'][len(df) - 1])
        if rename[city][2] == 'orphan':
            o = add_orphantotals(b, p, c, y, m, pq, cpq, csq)
        else:
            t = add_totals(b, p, c, y, m, pq, cpq, csq)
        count += 1
