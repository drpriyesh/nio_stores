import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NIOstores.settings')
django.setup()

from nio_stores.models import dataYear, dataMonth, Province, City, Brand, StoreAddress, Store, countryMonthlySold, countryStoreQty,countryParcs,cityMonthlySold,isOrphan, cityParcs
from province_city_pop import pcpop
from add_functions2 import add_dataYear, add_dataMonth, add_province, add_city, add_brand, add_countryParcs, \
    add_isOrphan, add_countryMonthlySold, add_cityMonthlySold, add_cityParcs
import pandas as pd

# m = dataMonth.objects.get_or_create(month=7)[0]
# cityms = cityParcs.objects.filter(month=m, parcsqty=0)
# print(list(cityParcs.objects.filter(month=m, parcsqty=0).values()))
# cityms.delete()


dataYear.objects.filter().delete()
dataMonth.objects.filter().delete()
Province.objects.filter().delete()
City.objects.filter().delete()
Brand.objects.filter().delete()
Store.objects.filter().delete()
countryStoreQty.objects.filter().delete()
StoreAddress.objects.filter().delete()
countryParcs.objects.filter().delete()
cityMonthlySold.objects.filter().delete()
isOrphan.objects.filter().delete()
cityParcs.objects.filter().delete()
countryMonthlySold.objects.filter().delete()
