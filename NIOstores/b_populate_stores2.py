# 品牌 Brand
# 省市 Province
# 城市 City
# 城市层级 City Level
# 区域 Region
# 门店名称 Name
# 门店地址 Address
# 门店功能 Function/Type
# 经营资质 Franchise or self
# 商业形态 Area Industry
# 门店位置 Location
# 服务类型 Service Type
# 重复(同一个地址两家店，NIO为sale + aftersales， 理想为维修+钣喷, tesla为自营+授权(钣喷)）Shared address
# 交付+服务(+销售) Delivery and service
# 展厅+店 Showrooms and shops
#
#
# 辅助列 secondary column
# 编号 Numbering
import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NIOstores.settings')
django.setup()
from nio_stores.models import Store
from add_functions2 import add_dataYear, add_dataMonth, add_province, add_city, add_brand, add_storeaddress, add_store, add_countryStoreQty
import pandas as pd



translation = {
'品牌':'brand',
'省市':'province',
'城市':'city',
'城市层级':'level',
'区域': '区域',
'门店名称':'name',
'门店地址':'address',
'门店功能':'type_0',
'经营资质':'f_s',
'商业形态':'area',
'门店位置':'zone',
'服务类型':'type_1',
'重复(同一个地址两家店，NIO为sale + aftersales， 理想为维修+钣喷, tesla为自营+授权(钣喷)）':'repeat',
'重复':'repeat',
'交付+服务(+销售)':'delivery',
'Unnamed 15':'Unnamed 15',
'Unnamed 16':'Unnamed 16',
'新店':'new store',
'展厅+店':'showroom',
'辅助列':'aux',
'编号':'serial'
}



year = input('Enter year of data in 4 digit format (20XX):') or '2021'
month = input('Enter month of data in number format (1 to 12):') or '7'
print(month, year)

if len(month) == 1:
    month = '0'+month
else:
    pass



filename = '2021' + month
print(filename)
df = pd.read_excel (r'/Users/NIO2/NIOstores/NIOstores/data/'+ filename +'/consolidated/' + filename + '.xlsx')
df.rename(translation, axis='columns',inplace=True)
df_stores = df.to_dict('records')
# year, month, province, city, brand, address, postcode, long, lat, store_id, name, type_0, type_1, franch_self, area, zone, repeat, delivery, showroom, aux, serial
print(df)

y = add_dataYear(year)
m = add_dataMonth(month)


def pop_store():
    count = 1
    for store in df_stores:
        print(count, store)
        count+=1
        p = add_province(store['province'])

        c = add_city(store['city'], p, store['level'])
        b = add_brand(store['brand'])
        a = add_storeaddress(store['address'])
        name = store['name']
        type_0 = store['type_0']
        type_1 = store['type_1']
        f_s = store['f_s']
        area = store['area']
        repeat = store['repeat']
        if "delivery" in store:
            delivery = store['delivery']
        else:
            delivery = ''
        if "showroom" in store:
            showroom = store['showroom']
        else:
            showroom = ''
        zone = store['zone']
        aux = store['aux']
        serial = store['serial']

        s = add_store(y, m, p, c, b, a, name, type_0, type_1, f_s, area, zone, repeat, delivery, showroom, aux, serial)

def pop_storetotals():
    brands = ['理想','小鹏','TESLA','NIO','理想','VW','TOYOTA']

    for brand in brands:
        b = add_brand(company_cn=brand)

        csq = len(list(Store.objects.filter(brand=b, month=m).values()))
        print(brand, csq)
        acsq=add_countryStoreQty(b, y, m, csq)


# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    pop_store()
    pop_storetotals()

