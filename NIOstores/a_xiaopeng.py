import requests
import pandas as pd
import os
import time
import sys
from pathfinder import pathfinder, picklepath
from province_city_pop import pcpop
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
import cookiejar
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
#
#
# path2 = sys.argv[1]

company = 'Xiaopeng'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def xiaopeng(path):
    # cookies = {
    #     'csrfToken': 'y6vLXAf-KUkd2uKwsZJGqCr7',
    #     'b134ba8dc28f178811789df957e11f7d': 'edb2f1c3007c882fb086535082940d36',
    #     'Hm_lvt_e50e47b9abfec85043aeff1c109832d0': '1626055849',
    #     'Hm_lpvt_e50e47b9abfec85043aeff1c109832d0': '1626075912',
    #     'acw_tc': 'a3b5399b16260744621883316ee9be5beef21b74c03f8d600978930223',
    # }
    #
    # headers = {
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0',
    #     'Accept': 'application/json, text/plain, */*',
    #     'Accept-Language': 'en-GB,en;q=0.5',
    #     'Content-Type': 'application/json;charset=utf-8',
    #     'Origin': 'https://www.xiaopeng.com',
    #     'Connection': 'keep-alive',
    #     'Referer': 'https://www.xiaopeng.com/pengmetta.html?forcePlat=h5',
    #     'TE': 'Trailers',
    # }
    cookies = cookiejar.xiaopeng()['cookies']
    headers = cookiejar.xiaopeng()['headers']
    url = cookiejar.xiaopeng()['url']
    data = '{"_csrf":"mkHiFWXA-tyyA5fhYXyiisyTEgtFwpmC-Jjs"}'

    response = requests.post(url, headers=headers, cookies=cookies, data=data)
    r2 = []
    data = []

    count = 0
    for dealer in response.json()['data']:
        count += 1

        datadict = {}
        datadict2 = {}

        city = dealer['cityName']
        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'

        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']

        datadict['1_dealerID'] = dealer['id']
        datadict['2_name'] = dealer['storeName']
        if int(dealer['storeType']) is 1:
            type = 'sales'
        elif int(dealer['storeType']) is 2:
            type = 'service'
        elif int(dealer['storeType']) is 3:
            type = 'sales and service'
        datadict['3_type_0'] = type
        datadict['3_type_1'] = dealer['storeType']
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        datadict['6_address'] = dealer['address']
        datadict['7_hours_week'] = dealer['businessHours']
        datadict['7_hours_weekend'] = dealer['weekendBusinessHours']
        datadict['8_tel'] = dealer['mobile']

        r2.append(datadict)
        datadict2 = {**datadict, **dealer}
        data.append(datadict2)

    df = pd.DataFrame(data)
    df2 = pd.DataFrame(r2)
    print(len(df), df)
    print(len(df2), df2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.astype(str).drop_duplicates(keep=False, inplace=True)
    print(len(df), df)
    print(len(df2), df2)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    xiaopeng(path)


# id 57
# siteCode CH
# storeName 小鹏汽车南京水游城体验中心
# businessHours 10:00-22:00
# storeType 1
# storeTypeName None
# storeTypeArr [1]
# storeStatus 1
# cn 0
# province 11
# provinceCode 320000
# city 109
# cityCode 320100
# district 1151
# districtCode 320104
# address 江苏省南京市秦淮区建康路1号南京鹏欣水游城F1-W07
# shortAddress 南京水游城
# lng 118.785408
# lat 32.024095
# coverUrl cms/store/pic/2019/09-02/19-28-320553-1104749103.jpg
# pcCoverUrl
# h5CoverUrl
# priority 2
# mobile 025-86108800
# storeCode C932AAS01
# reserveStatus 1
# weekendBusinessHours 10:00-22:00
# createTime 1564124641000
# updateTime 1598681918000
# isDeleted 0
# createBy 农颖
# updateBy 郭娴
# resources []
# provinceName 江苏
# cityName 南京市
# districtName 秦淮区
# coverUrlAccess https://xps01.xiaopeng.com/cms/store/pic/2019/09-02/19-28-320553-1104749103.jpg
# pcCoverUrlAccess
# h5CoverUrlAccess
# distance 0
