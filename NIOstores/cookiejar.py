# headers/cookies and urls may need to be updated
# headers and cookies for the urls obtained using the website below:
# https://curl.trillworks.com/#python



def lixiang():
    url = 'https://api-web.lixiang.com/mall-unit-api/v1-0/service-centers'

    headers = {
        'authority': 'api-web.lixiang.com',
        'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
        'x-chj-sourceurl': 'https://www.lixiang.com/?chjchannelcode=102002',
        'x-chj-traceid': 'f1f5b0e8900d842a6e75e3e8caf72157',
        'sec-ch-ua-mobile': '?0',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
        'content-type': 'application/json',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'x-chj-metadata': '{"code":102003}',
        'origin': 'https://www.lixiang.com',
        'sec-fetch-site': 'same-site',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://www.lixiang.com/',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'cookie': 'share-uid=; X-CHJ-SourceUrl=https%3A%2F%2Fwww.lixiang.com%2F%3Fchjchannelcode%3D102002; X-LX-Deviceid=ab5b0f67-f4d8-baa0-f744-17330a17dbad; gr_user_id=e49d7303-c3d4-45ae-b45a-dd0c9d5557e3; sajssdk_2015_cross_new_user=1; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%2217ae1c64fab144-0467d73bb0d1b8-34647600-1296000-17ae1c64fac43c%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_landing_page%22%3A%22https%3A%2F%2Fwww.lixiang.com%2F%22%7D%2C%22%24device_id%22%3A%2217ae1c64fab144-0467d73bb0d1b8-34647600-1296000-17ae1c64fac43c%22%7D; uid=5ea5fbe5-a165-41d7-8c9b-7df23943bd25; sid=7ffc9302-9c19-4676-b9df-8f3ff9ae22d8; JSESSIONID=eq43Z-1i4xoZm3Jel_IRWcL9XZSitC1gbV2-sVlO; a3c49546c2476d21_gr_session_id=e502b0e8-0d53-403b-99a1-518426ad9f9b; a3c49546c2476d21_gr_session_id_e502b0e8-0d53-403b-99a1-518426ad9f9b=true',
    }

    cookies = ''
    context_dict = {'cookies': cookies, 'headers': headers, 'url': url}
    return context_dict

# def nio:
#     context_dict = {'cookies': cookies, 'headers': headers}
#     return context_dict

def nissanDF():
    url1 = 'https://api.dongfeng-nissan.com.cn/api/Nissan/GetNissanCity'
    url2 = 'https://www.dongfeng-nissan.com.cn/Nissan/ajax/Distributor/GetJsonDistributorList'
    cookies = {
        'HMF_CI': '82753aaacf829e585402857dedaf3b33fafc9e076e789a2cbe83c6cf6b70bd8e9f',
        '_smt_uid': '60ee6dc3.3903aa39',
        '_jc_uid': '60ee6dc3.3903aa39',
        '__clickidc': '162623840818578645',
        'cig.perm.1674701870': '{"userId":"a4f68aa2aad87d63aa12504525e0501a"}',
        'SC_ANALYTICS_GLOBAL_COOKIE': 'a123ab94385f417fa24c970afdf10789|True',
        'sc_ext_contact': 'a123ab94385f417fa24c970afdf10789|True',
        'NTKF_T2D_CLIENTID': 'guestAC22276C-D391-E052-4917-A35F8359EA21',
        '_ga_NY1GRZCSLF': 'GS1.1.1626238604.1.1.1626240372.0',
        '_ga': 'GA1.3.377467378.1626238414',
        'ASP.NET_SessionId': 'lrrurpntcsffedpxvxmgburb',
        'NO_PAGE_DURATION': '2021/7/18 16:03:32',
        'HMY_JC': 'f81ee6c809ea3a1d4f31c9d7276e80379e36736f895347762ed7f49ccc158c9edd,',
        'cig.sess.1674701870': '{"site_id":"1195943075","se":1626597216043,"vid":"6042cbd21b6609627b0299001a477cf7","sid":"436e1a4a75a817907b8fcb6c1269dff1","st":1626595415917,"l":"59d5d3f672c71be22d56d6412600f360","ready":true,"p":"t","pv":0}',
        '_gid': 'GA1.3.1769034262.1626595416',
        '_gat': '1',
        'no_screen': '1440%7C900',
        'sc_ext_session': '0hytkyr1teev0a33stopvuun',
        'Place': '%7B%22localProvince%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%2C%22province%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%2C%22city%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%7D',
    }

    headers = {
        'Connection': 'keep-alive',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.dongfeng-nissan.com.cn',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://www.dongfeng-nissan.com.cn/buy/find-dealer',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    }
    context_dict = {'cookies': cookies, 'headers': headers, 'url1': url1, 'url2': url2}
    return context_dict

def nissanZZ():
    url1 = "https://www.zznissan.com.cn/nissan/index.php/support/dealer_inquiry"
    url2 = 'https://www.zznissan.com.cn/nissan/index.php/support/ajax_city_bypid'
    url3 = 'https://www.zznissan.com.cn/nissan/index.php/support/ajax_jxs_point'

    cookies = {
        'cookiesession1': '678B2873CDEFGHIJKLMNOPQRSTU07A95',
        'BIGipServerguanwang': '33663168.20480.0000',
        '_gscu_313507685': '264266921mu8ux13',
        '_gscbrs_313507685': '1',
        'Hm_lvt_7da60c2f4a62e82d32fba1fd07d28f80': '1626238534,1626239640,1626426694',
        'ttge_session': 'bbf4d60cbf504ade217941ac0f7f5be44bc448bc',
        'ci_session': '7e984c7e763275d774c5d33a0637f3ff3df4428d',
        '_gscs_313507685': 't26509152mjf6n613|pv:3',
        'Hm_lpvt_7da60c2f4a62e82d32fba1fd07d28f80': '1626511304',
    }

    headers = {
        'Connection': 'keep-alive',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Accept': 'text/plain, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.zznissan.com.cn',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://www.zznissan.com.cn/nissan/index.php/support/dealer_inquiry',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    }
    context_dict = {'cookies': cookies, 'headers': headers, 'url1': url1, 'url2': url2, 'url3': url3}
    return context_dict

def tesla():
    url1 = 'https://www.tesla.cn/cua-api/tesla-locations'
    url2 = 'https://www.tesla.cn/cua-api/tesla-location'
    headers1 = {
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Accept': 'application/json, text/plain, */*',
        'Referer': 'https://www.tesla.cn/findus?filters=store%2Cservice%2Csupercharger%2Cdestination%20charger%2Cbody%20repair%20center&location=cnsc8181',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    }

    headers2 = {
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Referer': 'https://www.tesla.cn/findus?filters=store%2Cservice%2Csupercharger%2Cdestination%20charger%2Cbody%20repair%20center&location=cnsc8181',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    }

    cookies = ''
    context_dict = {'cookies': cookies, 'headers1': headers1, 'headers2': headers2, 'url1': url1, 'url2': url2}

    return context_dict
# def toyotaFAW:
#     context_dict = {'cookies': cookies, 'headers': headers}
#     return context_dict

def toyotaGAC():
    url1 = 'https://www.gac-toyota.com.cn/2021/json/address.json'
    url2 = 'https://www.gac-toyota.com.cn/js/newprovincecitydealer/data/dealerData.js'

    headers = {
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Referer': 'https://www.gac-toyota.com.cn/buy/shopping/dealer-search',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    }
    cookies = ''

    context_dict = {'cookies': cookies, 'headers': headers, 'url1': url1, 'url2': url2}
    return context_dict

# def vwFAW:
#     context_dict = {'cookies': cookies, 'headers': headers}
#     return context_dict
# def vwSAIC:
#     context_dict = {'cookies': cookies, 'headers': headers}
#     return context_dict


def xiaopeng():
    url = 'https://www.xiaopeng.com/api/store/queryAll'
    cookies = {
        'csrfToken': 'y6vLXAf-KUkd2uKwsZJGqCr7',
        'b134ba8dc28f178811789df957e11f7d': 'edb2f1c3007c882fb086535082940d36',
        'Hm_lvt_e50e47b9abfec85043aeff1c109832d0': '1626055849',
        'Hm_lpvt_e50e47b9abfec85043aeff1c109832d0': '1626075912',
        'acw_tc': 'a3b5399b16260744621883316ee9be5beef21b74c03f8d600978930223',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-GB,en;q=0.5',
        'Content-Type': 'application/json;charset=utf-8',
        'Origin': 'https://www.xiaopeng.com',
        'Connection': 'keep-alive',
        'Referer': 'https://www.xiaopeng.com/pengmetta.html?forcePlat=h5',
        'TE': 'Trailers',
    }
    context_dict = {'cookies': cookies, 'headers': headers, 'url': url}
    return context_dict