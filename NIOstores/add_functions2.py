

from nio_stores.models import dataYear, dataMonth, Province, City, Brand, StoreAddress, Store, countryMonthlySold, countryStoreQty, countryParcs, cityMonthlySold, isOrphan, cityParcs
def add_dataYear(year):

    y = dataYear.objects.get_or_create(year=year)[0]
    y.save()
    return y

def add_dataMonth(month):

    m = dataMonth.objects.get_or_create(month=month)[0]
    m.save()
    return m

def add_province(province):
    if province == '内蒙古':
        province = '内蒙古自治区'
    if province == '宁夏':
        province = '宁夏回族自治区'
    if province == '广西':
        province = '广西壮族自治区'
    if province == '新疆':
        province = '新疆维吾尔自治区'
    if province == '西藏':
        province = '西藏自治区'
    p = Province.objects.get_or_create(province=province)[0]
    p.save()
    return p

def add_city(city, province, level):

    print(province)
    if province == '内蒙古':
        province = '内蒙古自治区'
    if province == '宁夏':
        province = '宁夏回族自治区'
    if province == '广西':
        province = '广西壮族自治区'
    if province == '新疆':
        province = '新疆维吾尔自治区'
    if province == '西藏':
        province = '西藏自治区'
    if province == '黔西南':
        province = '贵州'
    if city == '黔西南':
        province = '贵州'
        city = '黔西南布依族苗族自治州'
    if city == '黔西南':
        city = '黔西南布依族苗族自治州'
    if city == '黔东南':
        city = '黔东南苗族侗族自治州'
        province = '贵州'
    if city == '黔东南苗族侗族自治':
        city = '黔东南苗族侗族自治州'
        province = '贵州'
    if city == '黔东南苗族侗族自治区':
        city = '黔东南苗族侗族自治州'
        province = '贵州'
    if city == '阿拉善':
        city = '阿拉善盟'
    if city == '锡林郭':
        city = '锡林郭勒盟'
    if city == '西双版':
        city = '西双版纳傣族自治州'
        province = '云南'
    if city == '西双版纳':
        city = '西双版纳傣族自治州'
        province = '云南'
    if city == '西双版纳自治州':
        city = '西双版纳傣族自治州'
        province = '云南'
    if city == '湘西土':
        city = '湘西土家族苗族自治州'
        province = '湖南'
    if city == '湘西':
        city = '湘西土家族苗族自治州'
        province = '湖南'
    if city == '香港特':
        city = '香港特别行政區'
    if city == '红河哈':
        city = '红河哈尼族彝族自治州'
    if city == '德宏傣':
        city = '德宏傣族景颇族自治州'
        province = '云南'
    if city == '德宏':
        city = '德宏傣族景颇族自治州'
        province = '云南'
    if city == '德宏州':
        city = '德宏傣族景颇族自治州'
        province = '云南'
    if city == '延边朝':
        city = '延边朝鲜族自治州'
        province = '吉林'
    if city == '延边':
        city = '延边朝鲜族自治州'
        province = '吉林'
    if city == '巴音郭':
        city = '巴音郭楞蒙古自治州'
    if city == '大兴安':
        city = '大兴安岭地区'
        province = '黑龙江'
    if city == '大兴安岭':
        city = '大兴安岭地区'
        province = '黑龙江'
    if city == '凉山彝':
        city = '凉山彝族自治州'
    if city == '兴安':
        city = '兴安盟'
    if city == '黔南州':
        city = '黔南布依族苗族自治州'
        province = '贵州'
    if city == '黔南':
        city = '黔南布依族苗族自治州'
        province = '贵州'
    if city == '锡林郭勒':
        province = '内蒙古自治区'
        city='锡林郭勒盟'
    if city == '义乌':
        province = '浙江'
        city='金华'
        level='一线'
    if city == '慈溪':
        province = '浙江'
        city='金华'
        level='一线'
    province = add_province(province=province)
    print(province, city)
    c = City.objects.get_or_create(city=city, province=province)[0]
    c.level=level
    c.save()
    return c

def add_brand(company_cn):

    b = Brand.objects.get_or_create(company_cn=company_cn)[0]
    # b.company_en = company_en
    b.save()
    return b

def add_storeaddress(address):

    a = StoreAddress.objects.get_or_create(address=address)[0]
    a.save()
    return a

def add_store(year, month, province, city, brand, address, name, type_0, type_1, f_s, area, zone, repeat, delivery, showroom, aux, serial):
    # postcode, long, lat, store_id,

    s = Store.objects.get_or_create(year=year, month=month, province=province, city=city, brand=brand, address=address, serial=serial, name = name,type_0 = type_0,type_1 = type_1,f_s = f_s, area = area,zone = zone, repeat = repeat, delivery = delivery, showroom = showroom, aux = aux)[0]
    ## s.postcode = postcode
    ## s.long = long
    ## s.lat = lat
    ## s.store_id = store_id
    # s.name = name
    # s.type_0 = type_0
    # s.type_1 = type_1
    # s.f_s = f_s
    # s.area = area
    # s.zone = zone
    # s.repeat = repeat
    # s.delivery = delivery
    # s.showroom = showroom
    # s.aux = aux
    # s.serial = serial
    s.save()
    return s

# def add_ysbc(c, b, q, r, ap, y):
#     ysbc = yearlySalesbycity.objects.get_or_create(city=c, brand=b, year=y)[0]
#     ysbc.quantity = q
#     ysbc.revenue = r
#     ysbc.average_price = ap
#     ysbc.save()
#     return ysbc

# def add_ysbp(p, b, q, r, ap, y):
#     ysbp = yearlySalesbyprovince.objects.get_or_create(province=p, brand = b, year=y)[0]
#     ysbp.quantity = q
#     ysbp.revenue = r
#     ysbp.average_price = ap,
#     ysbp.save()
#     return ysbp

# def add_msbc(c, b, q, r, ap, y, m):
#     msbc = monthlySalesbycity.objects.get_or_create(city=c, brand=b, year=y, month=m)[0]
#     msbc.quantity = q
#     msbc.revenue = r
#     msbc.average_price = ap,
#     msbc.save()
#     return msbc

# def add_msbp(p, b, q, r, ap, y, m):
#     msbp = monthlySalesbyprovince.objects.get_or_create(province=p, brand=b, year=y, month=m)[0]
#     msbp.quantity = q
#     msbp.revenue = r
#     msbp.average_price = ap
#     msbp.save()
#     return msbp

# def add_totals(b, p, c, y, m, pq, cpq, csq):
#     t = Totals.objects.get_or_create(city=c, province=p, brand=b, year=y, month=m)[0]
#     t2 = Totals.objects.filter(brand=b, year=y, month=m)
#     t2.update(countryparcsqty = cpq)
#     t2.update(countrystoreqty = csq)
#     t.parcsqty = pq
#     t.save()
#     return t
# #
# def add_orphantotals(b, p, c, y, m, pq, cpq, csq):
#     o = orphanTotals.objects.get_or_create(city=c, province=p, brand=b, year=y, month=m)[0]
#     o.partsqty = pq
#     o2 = Totals.objects.filter(brand=b, year=y, month=m)
#     o2.update(countrypartsqty = cpq)
#     o2.update(countrystoreqty = csq)
#     o.save()
#     return o

# def add_monthlysales(b, p, c, y, m, ms, cms):
#     t = Totals.objects.get_or_create(city=c, province=p, brand=b, year=y, month=m)[0]
#     t2 = Totals.objects.filter(brand=b, year=y, month=m)
#     t2.update(countrymonthlysold = cms)
#     t.monthlysold = ms
#     t.save()
#
#     return t

# def add_orphanmonthlysales(b, p, c, y, m, ms, cms):
#     o = orphanTotals.objects.get_or_create(city=c, province=p, brand=b, year=y, month=m)[0]
#     o2 = Totals.objects.filter(brand=b, year=y, month=m)
#     o2.update(countrymonthlysold = cms)
#     o.monthlysold = ms
#     o2.countrymonthlysold = cms
#     o.save()
#
#     return o
def add_countryStoreQty(b, y, m, csq):
    c=countryStoreQty.objects.get_or_create(brand=b, year=y, month=m)[0]
    c.countrystoreqty=csq
    c.save()
    return c

def add_countryParcs(b, y, m, cpq):
    cp=countryParcs.objects.get_or_create(brand=b, year=y, month=m)[0]
    # cp2 = countryParcs.objects.filter(brand=b, year=y, month=m)
    # c.save()
    cp.countryparcsqty=cpq
    # cp2.update(countryparcsqty=cpq)
    cp.save()
    return cp

def add_cityParcs(b, p, c, y, m, pq, o):
    cp=cityParcs.objects.get_or_create(brand=b, year=y, month=m, city=c, province=p, isorphan=o)[0]
    # cp2 = cityParcs.objects.filter(brand=b, year=y, month=m, city=c, province=p, isorphan=o)
    # cp.save()
    # cp2.update(parcsqty=pq)
    cp.parcsqty=pq
    cp.save()
    return cp



def add_countryMonthlySold(b, y, m, cms):
    c=countryMonthlySold.objects.get_or_create(brand=b, year=y, month=m)[0]
    c.countrymonthlysold=cms
    c.save()
    return c
def add_cityMonthlySold(b, p, c, y, m, ms, o):
    cms=cityMonthlySold.objects.get_or_create(brand=b, year=y, month=m, city=c, province=p, isorphan=o)[0]
    cms2=cityMonthlySold.objects.filter(brand=b, year=y, month=m, city=c, province=p, isorphan=o)
    cms2.update(citymonthlysold=ms)
    # cms.citymonthlysold=ms
    cms.save()
    return cms


def add_isOrphan(b, p, c, y, m, o):
    i = isOrphan.objects.get_or_create(brand=b, year=y, month=m, city=c, province=p)[0]
    i.isorphan = o
    i.save()
    return i

# def add_Totals(b, p, c, y, m, countryMonthlySold, countryStoreQty, countryParcs, cityMonthlySold, cityParcs, isOrphan):
#     t=Totals.objects.get_or_create(brand=b, year=y, month=m, city=c, province=p, countrymonthlysold=countryMonthlySold, countrystoreqty=countryStoreQty, countryparcsqty=countryParcs, monthlysold=cityMonthlySold, parcsqty=cityParcs, isorphan=isOrphan)[0]
#     t.save()
#     return t