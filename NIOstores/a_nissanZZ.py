import requests
import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import re
import pandas as pd
import json
import cookiejar
from pathfinder import pathfinder, picklepath
from province_city_pop import pcpop
import time
import sys
import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
#
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
# path2 = sys.argv[1]

company = 'Nissan_ZZ'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def nissanzz(path):
    # url = "https://www.zznissan.com.cn/nissan/index.php/support/dealer_inquiry"
    url1 = cookiejar.nissanZZ()['url1']
    context = ssl._create_unverified_context()
    headers = {}
    headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17'

    request = urllib.request.Request(url1, headers=headers)
    html = urlopen(request, context=context)

    soup = BeautifulSoup(html, 'lxml')

    province_ids = soup.find_all('option') # sort through the province id's to get 'pid'
    pids = []
    pattern = "(\d+)"
    for line in province_ids:
        s = re.search(pattern, str(line), flags=0)
        if s:
            pid = s.group(1)
            pids.append(pid)

    # cookies = {
    #     'cookiesession1': '678B2873CDEFGHIJKLMNOPQRSTU07A95',
    #     'BIGipServerguanwang': '33663168.20480.0000',
    #     '_gscu_313507685': '264266921mu8ux13',
    #     '_gscbrs_313507685': '1',
    #     'Hm_lvt_7da60c2f4a62e82d32fba1fd07d28f80': '1626238534,1626239640,1626426694',
    #     'ttge_session': 'bbf4d60cbf504ade217941ac0f7f5be44bc448bc',
    #     'ci_session': '7e984c7e763275d774c5d33a0637f3ff3df4428d',
    #     '_gscs_313507685': 't26509152mjf6n613|pv:3',
    #     'Hm_lpvt_7da60c2f4a62e82d32fba1fd07d28f80': '1626511304',
    # }
    #
    # headers = {
    #     'Connection': 'keep-alive',
    #     'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    #     'Accept': 'text/plain, */*; q=0.01',
    #     'X-Requested-With': 'XMLHttpRequest',
    #     'sec-ch-ua-mobile': '?0',
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    #     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    #     'Origin': 'https://www.zznissan.com.cn',
    #     'Sec-Fetch-Site': 'same-origin',
    #     'Sec-Fetch-Mode': 'cors',
    #     'Sec-Fetch-Dest': 'empty',
    #     'Referer': 'https://www.zznissan.com.cn/nissan/index.php/support/dealer_inquiry',
    #     'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    # }
    headers = cookiejar.nissanZZ()['headers']
    cookies = cookiejar.nissanZZ()['cookies']


    # data_cityid = {
    #   'pid': '1'
    # }

    cid = {}
    cid_list = []
    url2 = cookiejar.nissanZZ()['url2']
    response_cityid = requests.post(url2, headers=headers, cookies=cookies, data={'pid': pids[0]})
    count = 0
    change_p = 0
    city_index = {}
    for p in pids:
        cid = {}
        if count > 0:
            try:
                change_p = int(pids[count])-int(pids[count-1])
            except IndexError:
                change_p = 0
        count += 1
        if change_p > 0:
            response_cityid = requests.post(url2, headers=headers, cookies=cookies, data={'pid': p})
        for c in response_cityid.json():
            print(p, c)
            cid = {}
            city_index[c['city_id']] = c['city_name']
            cid = {'jxs': '', 'province': p, 'city': c['city_id']}
            cid_list.append(cid)


    # need to replace 'city' with items from response_cityid
    print(cid_list)
    # data = {
    #   'jxs': '',
    #   'province': '1',
    #   'city': '2'
    # }
    r = []
    r2 = []

    count = 0
    sub_count = 0
    for data in cid_list:
        url3 = cookiejar.nissanZZ()['url3']
        response = requests.post(url3, headers=headers, cookies=cookies, data=data)
        # print(response.json())
        city = city_index[data['city']]

        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']

        count += 1

        for dealer in response.json():

            datadict = {}
            datadict2 = {}

            datadict['1_dealerID'] = dealer['dealer_id']
            datadict['2_name'] = dealer['dealer_name']
            datadict['3_type_0'] = 'sales and service'
            datadict['3_type_1'] = ''
            datadict['4_province'] = p_norm
            datadict['5_city'] = c_norm
            datadict['5_city_tier'] = t_norm
            datadict['6_address'] = dealer['dealer_address']
            datadict['7_hours'] = ''
            datadict['8_tel_0'] = dealer['dealer_tel']
            datadict['8_tel_1'] = dealer['dealer_fwtel']
            r2.append(datadict)
            datadict = dealer


            # count = 0
            # if dealer['CertificationLabels']:
            #     for item in dealer['CertificationLabels']:
            #
            #         head = ('3_'+ str(count) + '_type')
            #         dealer[head] = item
            #         count += 1
            print(datadict)
            sub_count += 1
            datadict2 = {**datadict, **dealer}
            r.append(datadict2)


    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.drop_duplicates(keep=False, inplace=True)
    df2.drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    nissanzz(path)





# [{'dealer_id': '662', 'dealer_number': '2222403', 'dealer_name': '安庆日升汽贸有限公司', 'dealer_pic': None, 'dealer_introduce': None, 'dealer_tel': '13905560097 ', 'dealer_fw'dealer_manage': '赵飞', 'dealer_manage_tel': '15855633337', 'dealer_fax': '', 'dealer_zipcode': '', 'dealer_address': '安庆市经开区乌岭迎宾工业园1#楼15-18号门面', 'dealer_levt': '117.052396', 'dealer_lon': '30.568688', 'province_id': '1', 'city_id': '2', 'dealer_info': None, 'dealer_disabled': '1', 'is_del': '0', 'dealer_parent_id': '0', 'websize_main': '1', 'websize_brand': '0', 'websize_province': '0', 'websize_dealer': '0', 'websize_main_ids': '0', 'websize_brand_ids': None, 'websize_province_ids': '', 'websize_dealer_ids': '-1', 'websize_from': '0', 'websize_from_id': '0', 'websize_inserttime': '2021-03-05 10:23:26', 'short_title': '安庆日升', 'public_key': '', 'private_key': '603eaef19', 'dealer_license': '', 'dealer_recommend_num': '0', 'dealer_dma_id': '21099', 'sort': '0'}]


# function
# getDealer(province_name, city_name)
# {
# $.ajax({
#     type: POST,
#     url: '//www.zznissan.com.cn/index.php/support/ajax_api_area',
#     data: '&province=' + province_name + '&city=' + city_name,
#     dataType: 'text',
#     success: function(data){
#         var obj = eval("(" + data + ")");
#
# var
# list = obj.list;
#
# var
# jxs = '';
# var
# province = obj.province_id;
# var
# city = obj.city_id;
#
# $('#province_id').find("option[value=" + province + "]").attr("selected", true);
# getCityDefault(city);
#
# // $('#city_id').find("option[value=" + city + "]").attr("selected", true);
# JxsPoint(jxs, province, city);
# }, error: function(data)
# {
#     alert(失败);
# }
# });
# }
