import cookiejar
import requests
import pandas as pd
from bs4 import BeautifulSoup
import json
from province_city_pop import pcpop
import time
import sys
import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
from pathfinder import pathfinder, picklepath
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
# path2 = sys.argv[1]

company = 'Tesla'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def tesla(path):
    # headers1 = {
    #     'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    #     'Accept': 'application/json, text/plain, */*',
    #     'Referer': 'https://www.tesla.cn/findus?filters=store%2Cservice%2Csupercharger%2Cdestination%20charger%2Cbody%20repair%20center&location=cnsc8181',
    #     'sec-ch-ua-mobile': '?0',
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    # }
    cookies = cookiejar.tesla()['cookies']
    url1 = cookiejar.tesla()['url1']
    url2 = cookiejar.tesla()['url2']
    headers1 = cookiejar.tesla()['headers1']
    headers2 = cookiejar.tesla()['headers2']
    params = (
        ('translate', 'zh_CN'),
        ('map', 'baidu'),
    )

    get_loc_ids = requests.get(url1, headers=headers1, params=params)

    loc_id_list = []
    for loc in get_loc_ids.json():
        if ('store' in str(loc) or 'bodyshop' in str(loc) or 'body repair center' in str(loc) or 'service' in str(loc) or 'delivery' in str(loc)):
            loc_id_list.append(loc['location_id'])
            print(loc)

    print(loc_id_list)

    # headers2 = {
    #     'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    #     'Referer': 'https://www.tesla.cn/findus?filters=store%2Cservice%2Csupercharger%2Cdestination%20charger%2Cbody%20repair%20center&location=cnsc8181',
    #     'sec-ch-ua-mobile': '?0',
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    # }

    r = []
    r2 = []

    for loc in loc_id_list:

        datadict = {}
        datadict2 = {}
        params = (
            ('id', loc),
            ('map', 'baidu'),
        )
        response = requests.get(url2, headers=headers2, params=params)
        # print(response.json())
        dealer = response.json()

        datadict['1_dealerID'] = dealer['nid']
        datadict['2_name'] = dealer['common_name']

        type = dealer['location_type']
        if ('store' not in str(type)):
            type = 'service'
            datadict['9_franch./self-own'] = 'self'

        elif ('store' in str(type)) and ('bodyshop' in str(type) or 'body repair center' in str(type) or 'service' in str(
                type) or 'delivery' in str(type)):
            type = 'sales and service'


        elif ('store' in str(type)) and ('bodyshop' not in str(type) and 'body repair center' not in str(type) and 'service' not in str(
                type) and 'delivery' not in str(type)):
            type = 'sales'





        datadict['3_type_0'] = type
        datadict['3_type_1'] = dealer['location_type']
        try:
            normal = pcpop(dealer['city'])
        except TypeError:
            normal['city'] = dealer['city']
            normal['province'] = dealer['city']
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = dealer['city']
            normal['province'] = dealer['city']
            normal['tier'] = '其他'

        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        datadict['6_address'] = dealer['address_line_1']+dealer['address_line_2']
        html = dealer['hours']
        soup = BeautifulSoup(html, 'lxml')

        ptag = soup.find_all('p')
        hcount = 0
        for tag in ptag:
            head = '7_hours_' + str(hcount)
            dealer[head] = tag.text
            hcount += 1

            # try:
            #     head  = '7_hours_' + tag.strong.text
            #     datadict[head] = tag.text.replace(tag.strong.text,'')
            # except AttributeError:
            #     datadict[head] = '7_hours'

        tel_no = dealer['sales_phone']
        tcount = 0
        for tel in tel_no:
            tel_head = '8_tel_' + str(tcount)
            datadict[tel_head] = tel['label'] + ':' + tel['number']
            tcount += 1

        if ('bodyshop' in str(dealer['location_type'])) and ('body repair center' not in str(dealer['location_type'])):
            datadict['9_franch./self-own'] = 'franch'
        elif 'sales' in type:
            datadict['9_franch./self-own'] = 'self'

        if not datadict['9_franch./self-own']:
            datadict['9_franch./self-own'] = 'self'

            # tel_head = '8_tel_' + tel['label']
            # datadict[tel_head] = tel['number']
            ## if tel['number']:
            ##     datadict[tel_head] = tel['number']
            ## else:
            ##     datadict[tel_head] = ''
        r2.append(datadict)
        print(datadict)
        datadict2 = {**datadict, **dealer}
        r.append(datadict2)

    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.astype(str).drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    tesla(path)