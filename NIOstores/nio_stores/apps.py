from django.apps import AppConfig


class NioStoresConfig(AppConfig):
    name = 'nio_stores'
