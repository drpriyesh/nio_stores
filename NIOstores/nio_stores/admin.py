from django.contrib import admin
from django.apps import apps
from nio_stores.models import dataYear, dataMonth, Province, City, Brand, StoreAddress, Store, countryParcs, cityParcs, cityMonthlySold
from django.contrib import messages
from django.utils.translation import ngettext
from django.contrib.admin import SimpleListFilter

# class DuplicateStoreAddressFilter(SimpleListFilter):
#
#     title = 'Duplicates'
#     parameter_name = 'address'
#
#     def lookups(self, request, model_admin):
#         return (('duplicates', 'Duplicates'),)
#     def queryset(self, request, queryset):
#         if not self.value():
#             return queryset
#         if self.value().lower() == 'duplicates':
#             addresses = StoreAddress.objects.all().values('address').distinct().order_by("address")
#             return queryset.filter().exclude(id__in=[StoreAddress.pk for StoreAddress in addresses])





@admin.action(description='Change selected to 黔南布依族苗族自治州')
def change_to_黔南布依族苗族自治州(self, request, queryset):
    updated = queryset.update(city=239)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)



@admin.action(description='Change selected to 黔西南布依族苗族自治州')
def change_to_黔西南布依族苗族自治州(self, request, queryset):
    updated = queryset.update(city=208)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)

@admin.action(description='Change selected to 黔东南苗族侗族自治州')
def change_to_黔东南苗族侗族自治州(self, request, queryset):
    updated = queryset.update(city=193)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 阿拉善盟')
def change_to_阿拉善盟(self, request, queryset):
    updated = queryset.update(city=269)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 锡林郭勒盟')
def change_to_锡林郭勒盟(self, request, queryset):
    updated = queryset.update(city=298)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 西双版纳傣族自治州 ')
def change_to_西双版纳傣族自治州 (self, request, queryset):
    updated = queryset.update(city=297)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 湘西土家族苗族自治州')
def change_to_湘西土家族苗族自治州(self, request, queryset):
    updated = queryset.update(city=252)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 香港特别行政區')
def change_to_香港特别行政區(self, request, queryset):
    updated = queryset.update(city=361)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 红河哈尼族彝族自治州')
def change_to_红河哈尼族彝族自治州(self, request, queryset):
    updated = queryset.update(city=197)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 德宏傣族景颇族自治州')
def change_to_德宏傣族景颇族自治州(self, request, queryset):
    updated = queryset.update(city=227)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 延边朝鲜族自治州')
def change_to_延边朝鲜族自治州(self, request, queryset):
    updated = queryset.update(city=306)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 巴音郭楞蒙古自治州')
def change_to_巴音郭楞蒙古自治州(self, request, queryset):
    updated = queryset.update(city=302)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 大兴安岭地区')
def change_to_大兴安岭地区(self, request, queryset):
    updated = queryset.update(city=326)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 凉山彝族自治州')
def change_to_凉山彝族自治州(self, request, queryset):
    updated = queryset.update(city=170)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
@admin.action(description='Change selected to 兴安盟')
def change_to_兴安盟(self, request, queryset):
    updated = queryset.update(city=304)
    self.message_user(request, ngettext(
        '%d city was successfully changed.',
        '%d cities were successfully changed.',
        updated,
    ) % updated, messages.SUCCESS)
# 黔东南苗族侗族自治州 193
# 阿拉善盟 269
# 锡林郭勒盟 298
# 西双版纳傣族自治州
# 湘西土家族苗族自治州
# 香港特别行政區
# 红河哈尼族彝族自治州
# 德宏傣族景颇族自治州
# 延边朝鲜族自治州
# 巴音郭楞蒙古自治州
# 大兴安岭地区
# 凉山彝族自治州
# 兴安盟



class cityParcsAdmin(admin.ModelAdmin):
    actions = [
        change_to_黔南布依族苗族自治州,
        change_to_黔西南布依族苗族自治州,
        change_to_黔东南苗族侗族自治州,
        change_to_阿拉善盟,
        change_to_锡林郭勒盟,
        change_to_西双版纳傣族自治州,
        change_to_湘西土家族苗族自治州,
        change_to_香港特别行政區,
        change_to_红河哈尼族彝族自治州,
        change_to_德宏傣族景颇族自治州,
        change_to_延边朝鲜族自治州,
        change_to_巴音郭楞蒙古自治州,
        change_to_大兴安岭地区,
        change_to_凉山彝族自治州,
        change_to_兴安盟
        ]
    class Meta:
        # verbose_name_plural = 'User Sentences'
        ordering = ['city', 'brand', 'month', 'parcsqty']
    list_per_page = 100
    list_filter = ['brand']
    # search list
    search_fields = ['brand__company_cn','city__city','month__month']
    list_display = ('get_year', 'get_month', 'get_province', 'get_city', 'get_brand', 'parcsqty')
    def get_province(self, obj):
        return obj.province.province

    get_province.admin_order_field = 'province'  # Allows column order sorting
    get_province.short_description = 'province'  # Renames column head

    def get_city(self, obj):
        return obj.city.city

    get_city.admin_order_field = 'city'  # Allows column order sorting
    get_city.short_description = 'city'  # Renames column head

    def get_brand(self, obj):
        return obj.brand.company_cn

    get_brand.admin_order_field = 'brand'  # Allows column order sorting
    get_brand.short_description = 'brand'  # Renames column head

    def get_year(self, obj):
        return obj.year.year

    get_year.admin_order_field = 'year'  # Allows column order sorting
    get_year.short_description = 'year'  # Renames column head

    def get_month(self, obj):
        return obj.month.month

    get_month.admin_order_field = 'month'  # Allows column order sorting
    get_month.short_description = 'month'  # Renames column head
#


#
#
# from nio_stores.models import dataYear, dataMonth, Province, City, Brand, StoreAddress, Store, countryMonthlySold, countryStoreQty, cityMonthlySold, isOrphan
# class get_all():
#     def get_province(self, obj):
#         return obj.province.province
#     def get_city(self, obj):
#         return obj.city.city
#     def get_brand(self, obj):
#         return obj.brand.company_cn
#     def get_year(self, obj):
#         return obj.year.year
#     def get_month(self, obj):
#         return obj.month.month
#     def get_partsqty(self, obj):
#         return obj.partsqty.partsqty
#     def get_countrypartsqty(self, obj):
#         return obj.countrypartsqty.countrypartsqty
#     def get_countrystoreqty(self, obj):
#         return obj.countrystoreqty.countrystoreqty
#     def get_monthlysold(self, obj):
#         return obj.monthlysold.citymonthlysold
#     def get_countrymonthlysold(self, obj):
#         return obj.countrymonthlysold.countrymonthlysold
#     def get_isorphan(self, obj):
#         return obj.isorphan.isorphan
#     def get_address(self, obj):
#         return obj.address.address
#
# class dataYearAdmin(admin.ModelAdmin):
#     list_display = ('year',)
# class dataMonthAdmin(admin.ModelAdmin):
#     list_display = ('month',)
# class ProvinceAdmin(admin.ModelAdmin):
#     list_display = ('province',)
# class CityAdmin(admin.ModelAdmin):
#     list_display = ('city','province','level')
# class BrandAdmin(admin.ModelAdmin):
#     list_display = ('company_cn',)
class CityAdmin(admin.ModelAdmin):
    class Meta:
        # verbose_name_plural = 'User Sentences'
        ordering = ['city']
    list_per_page = 500
    list_filter = ['province__province','level', 'id']
    list_display = ('city','get_province','level', 'id')
    search_fields = [ 'city','province__province','level', 'id']

    def get_province(self, obj):
        return obj.province.province

    get_province.admin_order_field = 'province'  # Allows column order sorting
    get_province.short_description = 'province'  # Renames column head


class StoreAddressAdmin(admin.ModelAdmin):
    actions = [
        change_to_黔南布依族苗族自治州,
        change_to_黔西南布依族苗族自治州,
        change_to_黔东南苗族侗族自治州,
        change_to_阿拉善盟,
        change_to_锡林郭勒盟,
        change_to_西双版纳傣族自治州,
        change_to_湘西土家族苗族自治州,
        change_to_香港特别行政區,
        change_to_红河哈尼族彝族自治州,
        change_to_德宏傣族景颇族自治州,
        change_to_延边朝鲜族自治州,
        change_to_巴音郭楞蒙古自治州,
        change_to_大兴安岭地区,
        change_to_凉山彝族自治州,
        change_to_兴安盟
    ]

    class Meta:
        # verbose_name_plural = 'User Sentences'
        ordering = ['city', 'brand', 'month']

    list_per_page = 5000
    # search list
    search_fields = ['address']
    # list_filter = (DuplicateStoreAddressFilter,)
    list_display = ('address',)



class StoreAdmin(admin.ModelAdmin):

    actions = [
        change_to_黔南布依族苗族自治州,
        change_to_黔西南布依族苗族自治州,
        change_to_黔东南苗族侗族自治州,
        change_to_阿拉善盟,
        change_to_锡林郭勒盟,
        change_to_西双版纳傣族自治州,
        change_to_湘西土家族苗族自治州,
        change_to_香港特别行政區,
        change_to_红河哈尼族彝族自治州,
        change_to_德宏傣族景颇族自治州,
        change_to_延边朝鲜族自治州,
        change_to_巴音郭楞蒙古自治州,
        change_to_大兴安岭地区,
        change_to_凉山彝族自治州,
        change_to_兴安盟
        ]




    class Meta:
        # verbose_name_plural = 'User Sentences'
        ordering = ['city', 'brand', 'month']
    list_per_page = 500
    list_filter = ['brand', 'month__month']
    # search list
    search_fields = ['brand__company_cn','city__city','month__month', 'serial']
    list_display = ('get_year', 'get_month', 'get_province', 'get_city', 'get_brand', 'repeat', 'type_0', 'serial', 'get_address')
    raw_id_fields = ("city",)


    def get_type_0(self, obj):
        return obj.type_0.type_0
    get_type_0.admin_order_field = 'type_0'  # Allows column order sorting
    get_type_0.short_description = 'type_0'  # Renames column head

    def get_repeat(self, obj):
        return obj.repeat.repeat
    get_repeat.admin_order_field = 'repeat'  # Allows column order sorting
    get_repeat.short_description = 'repeat'  # Renames column head

    def get_address(self, obj):
        return obj.address.id
    get_address.admin_order_field = 'address_id'  # Allows column order sorting
    get_address.short_description = 'address_id'  # Renames column head
    
    def get_province(self, obj):
        return obj.province.province

    get_province.admin_order_field = 'province'  # Allows column order sorting
    get_province.short_description = 'province'  # Renames column head

    def get_city(self, obj):
        return obj.city.city

    get_city.admin_order_field = 'city'  # Allows column order sorting
    get_city.short_description = 'city'  # Renames column head

    def get_brand(self, obj):
        return obj.brand.company_cn

    get_brand.admin_order_field = 'brand'  # Allows column order sorting
    get_brand.short_description = 'brand'  # Renames column head

    def get_year(self, obj):
        return obj.year.year

    get_year.admin_order_field = 'year'  # Allows column order sorting
    get_year.short_description = 'year'  # Renames column head

    def get_month(self, obj):
        return obj.month.month

    get_month.admin_order_field = 'month'  # Allows column order sorting
    get_month.short_description = 'month'  # Renames column head
#

class cityMonthlySoldAdmin(admin.ModelAdmin):
    actions = [
        change_to_黔南布依族苗族自治州,
        change_to_黔西南布依族苗族自治州,
        change_to_黔东南苗族侗族自治州,
        change_to_阿拉善盟,
        change_to_锡林郭勒盟,
        change_to_西双版纳傣族自治州,
        change_to_湘西土家族苗族自治州,
        change_to_香港特别行政區,
        change_to_红河哈尼族彝族自治州,
        change_to_德宏傣族景颇族自治州,
        change_to_延边朝鲜族自治州,
        change_to_巴音郭楞蒙古自治州,
        change_to_大兴安岭地区,
        change_to_凉山彝族自治州,
        change_to_兴安盟
    ]


    class Meta:
        # verbose_name_plural = 'User Sentences'
        ordering = ['city', 'brand', 'month']

    list_per_page = 500
    list_filter = ['brand', 'month__month']
    # search list
    search_fields = ['brand__company_cn', 'city__city', 'month__month']
    list_display = (
    'get_year', 'get_month', 'get_province', 'get_city', 'get_brand', 'citymonthlysold')
    raw_id_fields = ("city",)



    def get_province(self, obj):
        return obj.province.province

    get_province.admin_order_field = 'province'  # Allows column order sorting
    get_province.short_description = 'province'  # Renames column head

    def get_city(self, obj):
        return obj.city.city

    get_city.admin_order_field = 'city'  # Allows column order sorting
    get_city.short_description = 'city'  # Renames column head

    def get_brand(self, obj):
        return obj.brand.company_cn

    get_brand.admin_order_field = 'brand'  # Allows column order sorting
    get_brand.short_description = 'brand'  # Renames column head

    def get_year(self, obj):
        return obj.year.year

    get_year.admin_order_field = 'year'  # Allows column order sorting
    get_year.short_description = 'year'  # Renames column head

    def get_month(self, obj):
        return obj.month.month

    get_month.admin_order_field = 'month'  # Allows column order sorting
    get_month.short_description = 'month'  # Renames column head


admin.site.register(StoreAddress, StoreAddressAdmin)
admin.site.register(cityMonthlySold, cityMonthlySoldAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(cityParcs, cityParcsAdmin)
admin.site.register(Store, StoreAdmin)
# # class TotalsAdmin(admin.ModelAdmin):
# #     list_display = ('get_year', 'get_month', 'get_province', 'get_city', 'get_brand', 'get_partsqty', 'get_countrypartsqty', 'get_countrystoreqty', 'get_monthlysold', 'get_countrymonthlysold', 'get_isorphan')
# #     get_all.get_province.admin_order_field = 'province'  # Allows column order sorting
# #     get_all.get_province.short_description = 'province'  # Renames column head
# #     get_all.get_city.admin_order_field = 'city'  # Allows column order sorting
# #     get_all.get_city.short_description = 'city'  # Renames column head
# #     get_all.get_brand.admin_order_field = 'brand'  # Allows column order sorting
# #     get_all.get_brand.short_description = 'brand'  # Renames column head
# #     get_all.get_year.admin_order_field = 'year'  # Allows column order sorting
# #     get_all.get_year.short_description = 'year'  # Renames column head
# #     get_all.get_month.admin_order_field = 'month'  # Allows column order sorting
# #     get_all.get_month.short_description = 'month'  # Renames column head
# #     get_all.get_partsqty.admin_order_field = 'partsqty'  # Allows column order sorting
# #     get_all.get_partsqty.short_description = 'partsqty'  # Renames column head
# #     get_all.get_countrypartsqty.admin_order_field = 'countrypartsqty'  # Allows column order sorting
# #     get_all.get_countrypartsqty.short_description = 'countrypartsqty'  # Renames column head
# #     get_all.get_countrystoreqty.admin_order_field = 'countrystoreqty'  # Allows column order sorting
# #     get_all.get_countrystoreqty.short_description = 'countrystoreqtyr'  # Renames column head
# #     get_all.get_monthlysold.admin_order_field = 'monthlysold'  # Allows column order sorting
# #     get_all.get_monthlysold.short_description = 'monthlysold'  # Renames column head
# #     get_all.get_countrymonthlysold.admin_order_field = 'countrymonthlysold'  # Allows column order sorting
# #     get_all.get_countrymonthlysold.short_description = 'countrymonthlysold'  # Renames column head
# #     get_all.get_isorphan.admin_order_field = 'isorphan'  # Allows column order sorting
# #     get_all.get_isorphan.short_description = 'isorphan'  # Renames column head
# #     get_all.get_address.admin_order_field = 'address'  # Allows column order sorting
# #     get_all.get_address.short_description = 'address'  # Renames column head
#
#
# # class orphanTotalsAdmin(admin.ModelAdmin):
# #     list_display = ('get_year', 'get_month', 'get_province', 'get_city', 'get_brand', 'partsqty', 'countrypartsqty', 'countrystoreqty', 'monthlysold', 'countrymonthlysold')
# #
# #     def get_province(self, obj):
# #         return obj.province.province
# #
# #     get_province.admin_order_field = 'province'  # Allows column order sorting
# #     get_province.short_description = 'province'  # Renames column head
# #
# #     def get_city(self, obj):
# #         return obj.city.city
# #
# #     get_city.admin_order_field = 'city'  # Allows column order sorting
# #     get_city.short_description = 'city'  # Renames column head
# #
# #     def get_brand(self, obj):
# #         return obj.brand.company_cn
# #
# #     get_brand.admin_order_field = 'brand'  # Allows column order sorting
# #     get_brand.short_description = 'brand'  # Renames column head
# #
# #     def get_year(self, obj):
# #         return obj.year.year
# #
# #     get_year.admin_order_field = 'year'  # Allows column order sorting
# #     get_year.short_description = 'year'  # Renames column head
# #
# #     def get_month(self, obj):
# #         return obj.month.month
# #
# #     get_month.admin_order_field = 'month'  # Allows column order sorting
# #     get_month.short_description = 'month'  # Renames column head
#
# admin.site.register(Store, StoreAdmin)
# # admin.site.register(Totals, TotalsAdmin)
# # admin.site.register(isOrphan, isOrphanAdmin)
# admin.site.register(Brand, BrandAdmin)
# admin.site.register(City, CityAdmin)
# admin.site.register(Province, ProvinceAdmin)
#
class ListAdminMixin(object):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields]
        super(ListAdminMixin, self).__init__(model, admin_site)


# all other models
models = apps.get_models()

for model in models:

    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass




#
#
#
#
# # get_all.get_province.admin_order_field = 'province'  # Allows column order sorting
# # get_all.get_province.short_description = 'province'  # Renames column head
# # get_all.get_city.admin_order_field = 'city'  # Allows column order sorting
# # get_all.get_city.short_description = 'city'  # Renames column head
# # get_all.get_brand.admin_order_field = 'brand'  # Allows column order sorting
# # get_all.get_brand.short_description = 'brand'  # Renames column head
# # get_all.get_year.admin_order_field = 'year'  # Allows column order sorting
# # get_all.get_year.short_description = 'year'  # Renames column head
# # get_all.get_month.admin_order_field = 'month'  # Allows column order sorting
# # get_all.get_month.short_description = 'month'  # Renames column head
# # get_all.get_partsqty.admin_order_field = 'partsqty'  # Allows column order sorting
# # get_all.get_partsqty.short_description = 'partsqty'  # Renames column head
# # get_all.get_countrypartsqty.admin_order_field = 'countrypartsqty'  # Allows column order sorting
# # get_all.get_countrypartsqty.short_description = 'countrypartsqty'  # Renames column head
# # get_all.get_countrystoreqty.admin_order_field = 'countrystoreqty'  # Allows column order sorting
# # get_all.get_countrystoreqty.short_description = 'countrystoreqtyr'  # Renames column head
# # get_all.get_monthlysold.admin_order_field = 'monthlysold'  # Allows column order sorting
# # get_all.get_monthlysold.short_description = 'monthlysold'  # Renames column head
# # get_all.get_countrymonthlysold.admin_order_field = 'countrymonthlysold'  # Allows column order sorting
# # get_all.get_countrymonthlysold.short_description = 'countrymonthlysold'  # Renames column head
# # get_all.get_isorphan.admin_order_field = 'isorphan'  # Allows column order sorting
# # get_all.get_isorphan.short_description = 'isorphan'  # Renames column head
# # get_all.get_address.admin_order_field = 'address'  # Allows column order sorting
# # get_all.get_address.short_description = 'address'  # Renames column head