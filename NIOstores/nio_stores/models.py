from django.db import models
import datetime
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.contrib import admin
import re




class dataYear(models.Model):
    start_year = 2020
    YEAR_CHOICES = [(y, y) for y in range(start_year, datetime.date.today().year + 1)]
    year = models.IntegerField(choices=YEAR_CHOICES, default=datetime.datetime.now().year, )
    def save(self, *args, **kwargs):
        self.slug = slugify(self.year)
        super(dataYear, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.year)

class dataMonth(models.Model):
    MONTH_CHOICE = [(m, m) for m in range(1, 13)]
    month = models.IntegerField(choices=MONTH_CHOICE, default=datetime.datetime.now().month, )
    def save(self, *args, **kwargs):
        self.slug = slugify(self.month)
        super(dataMonth, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.month)

class Province(models.Model):
    class Meta:
        verbose_name_plural = 'Provinces'

    TITLE_MAX_LENGTH = 128

    province = models.CharField(max_length=TITLE_MAX_LENGTH, unique=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.province)
        super(Province, self).save(*args, **kwargs)

    def __str__(self):
        return self.province


class City(models.Model):
    class Meta:
        verbose_name_plural = 'Cities'
        ordering = ['city']

    TITLE_MAX_LENGTH = 128
    # LEVEL_CHOICES = ['超一线', '一线', '二线', '三线', '其他', '港澳台']
    city = models.CharField(max_length=TITLE_MAX_LENGTH, unique=True)
    province = models.ForeignKey(Province, on_delete=models.CASCADE)
    level = models.CharField('城市层级', max_length=TITLE_MAX_LENGTH, null=True, blank=True) # , choices=LEVEL_CHOICES
    city_code = models.CharField(max_length=TITLE_MAX_LENGTH, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.city)
        super(City, self).save(*args, **kwargs)

    def __str__(self):
        return self.city


class Brand(models.Model):
    class Meta:
        verbose_name_plural = 'Companies'

    TITLE_MAX_LENGTH = 128

    company_en = models.CharField(max_length=TITLE_MAX_LENGTH)
    company_cn = models.CharField(max_length=TITLE_MAX_LENGTH, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.company_cn)
        super(Brand, self).save(*args, **kwargs)

    def __str__(self):
        return self.company_cn or ''

class StoreAddress(models.Model):
    TITLE_MAX_LENGTH = 512
    address = models.CharField(max_length=TITLE_MAX_LENGTH, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.address)
        super(StoreAddress, self).save(*args, **kwargs)

    def __str__(self):
        return self.address

class Store(models.Model):
    class Meta:
        verbose_name_plural = 'Stores'

    # TYPE_CHOICES = ['sales', 'service', 'sales and service']
    # FRANCHISE_CHOICES = ['franchised', 'self-owned']
    TITLE_MAX_LENGTH = 128
    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    province = models.ForeignKey(Province, verbose_name ='Province 省市', on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name ='City 城市', on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name ='Brand 品牌', on_delete=models.CASCADE)
    address = models.ForeignKey(StoreAddress, verbose_name ='Address 门店地址', on_delete=models.CASCADE)
    postcode = models.CharField(max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    long = models.FloatField(null=True, blank=True)
    lat = models.FloatField(null=True, blank=True)
    store_id = models.CharField(max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    name = models.CharField('Name 门店名称', max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    type_0 = models.TextField('Type_0 Store function 门店功能', null=True, blank=True) # , choices=TYPE_CHOICES
    type_1 = models.TextField('Service type 服务类型', null=True, blank=True)
    f_s = models.TextField('Franch/Self 经营资质', null=True, blank=True) # , choices = FRANCHISE_CHOICES
    area = models.TextField('Area type (e.g. mall) 商业形态', null=True, blank=True)
    zone = models.TextField('Zone type (e.g. CBD) 门店位置', null=True, blank=True)
    repeat = models.CharField('repeat', max_length=TITLE_MAX_LENGTH, null=True, blank=True)#重复(同一个地址两家店，NIO为sale + aftersales， 理想为维修+钣喷, tesla为自营+授权(钣喷)）
    delivery = models.CharField('Delivery 交付+服务(+销售)', max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    showroom = models.CharField('Showroom and sales 展厅+店', max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    aux = models.CharField('Aux 辅助列', max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    serial = models.CharField('Serial 编号', max_length=TITLE_MAX_LENGTH, null=True, blank=True)
    otherdata = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Store, self).save(*args, **kwargs)

    def __str__(self):
        return self.name or ''


# class yearlySalesbycity(models.Model):
#     class Meta:
#         verbose_name_plural = 'Yearly City Sales'
#
#     TITLE_MAX_LENGTH = 128
#
#     city = models.ForeignKey(City, on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
#     quantity = models.FloatField(null=True, blank=True)
#     revenue = models.FloatField(null=True, blank=True)
#     average_price = models.FloatField(null=True, blank=True)
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#
#
#
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.quantity)
#         super(yearlySalesbycity, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.quantity


# class yearlySalesbyprovince(models.Model):
#     class Meta:
#         verbose_name_plural = 'Yearly Province Sales'
#
#     TITLE_MAX_LENGTH = 128
#
#     province = models.ForeignKey(Province, on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
#     quantity = models.FloatField(null=True, blank=True)
#     revenue = models.FloatField(null=True, blank=True)
#     average_price = models.FloatField(null=True, blank=True)
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.quantity)
#         super(yearlySalesbyprovince, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.quantity


# class monthlySalesbycity(models.Model):
#     class Meta:
#         verbose_name_plural = 'Monthly City Sales'
#
#     TITLE_MAX_LENGTH = 128
#
#     city = models.ForeignKey(City, on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
#     quantity = models.FloatField(null=True, blank=True)
#     revenue = models.FloatField(null=True, blank=True)
#     average_price = models.FloatField(null=True, blank=True)
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#     month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.quantity)
#         super(monthlySalesbycity, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.quantity


# class monthlySalesbyprovince(models.Model):
#     class Meta:
#         verbose_name_plural = 'Monthly Province Sales'
#
#     TITLE_MAX_LENGTH = 128
#
#     province = models.ForeignKey(Province, on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
#     quantity = models.FloatField(null=True, blank=True)
#     revenue = models.FloatField(null=True, blank=True)
#     average_price = models.FloatField(null=True, blank=True)
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#     month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.quantity)
#         super(monthlySalesbyprovince, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.quantity




class countryMonthlySold(models.Model):

    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    countrymonthlysold = models.FloatField('Monthly sold per country', null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.countrymonthlysold)
        super(countryMonthlySold, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.countrymonthlysold)

class countryStoreQty(models.Model):

    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    countrystoreqty = models.FloatField('12_total_dealers', null=True, blank=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.countrystoreqty)
        super(countryStoreQty, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.countrystoreqty)

class countryParcs(models.Model):

    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    countryparcsqty = models.FloatField('11_total_sales', null=True, blank=True)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.countryparcsqty)
        super(countryParcs, self).save(*args, **kwargs) or ''

    def _str__(self):
        return str(self.countryparcsqty)

class cityMonthlySold(models.Model):

    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    province = models.ForeignKey(Province, verbose_name='Province 省市', on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name='City 城市', on_delete=models.CASCADE)
    citymonthlysold = models.FloatField('Monthly sold per city', null=True, blank=True)
    isorphan = models.BooleanField(default=False)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.citymonthlysold)
        super(cityMonthlySold, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.citymonthlysold)


class isOrphan(models.Model):
    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    province = models.ForeignKey(Province, verbose_name='Province 省市', on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name='City 城市', on_delete=models.CASCADE)
    isorphan = models.BooleanField(default=False)

    def __str__(self):
        return str(self.isorphan)


class cityParcs(models.Model):

    year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
    month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
    province = models.ForeignKey(Province, verbose_name='Province 省市', on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name='City 城市', on_delete=models.CASCADE)
    parcsqty = models.FloatField('parcsqty', null=True, blank=True)
    isorphan = models.ForeignKey(isOrphan, on_delete=models.CASCADE)
    def save(self, *args, **kwargs):
        self.slug = slugify(self.parcsqty)
        super(cityParcs, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.parcsqty)

# class Totals(models.Model):
#     class Meta:
#         verbose_name_plural = 'Total parcs on road to date'
#
#     TITLE_MAX_LENGTH = 128
#
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#     month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
#     province = models.ForeignKey(Province, verbose_name ='Province 省市', on_delete=models.CASCADE)
#     city = models.ForeignKey(City, verbose_name ='City 城市', on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, verbose_name ='Brand 品牌', on_delete=models.CASCADE)
#     parcsqty = models.ForeignKey(cityParcs, on_delete=models.CASCADE)
#     countryparcsqty = models.ForeignKey(countryParcs, on_delete=models.CASCADE)
#     countrystoreqty = models.ForeignKey(countryStoreQty, on_delete=models.CASCADE)
#     monthlysold = models.ForeignKey(cityMonthlySold, on_delete=models.CASCADE)
#     countrymonthlysold = models.ForeignKey(countryMonthlySold, on_delete=models.CASCADE)
#     isorphan = models.ForeignKey(isOrphan, on_delete=models.CASCADE)
#
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.brand.company_cn)
#         super(Totals, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.brand.company_cn


# class orphanTotals(models.Model):
#
#     class Meta:
#         verbose_name_plural = 'Orphan total parcs on road to date'
#
#     TITLE_MAX_LENGTH = 128
#
#     year = models.ForeignKey(dataYear, on_delete=models.CASCADE)
#     month = models.ForeignKey(dataMonth, on_delete=models.CASCADE)
#     province = models.ForeignKey(Province, verbose_name='Province 省市', on_delete=models.CASCADE)
#     city = models.ForeignKey(City, verbose_name='City 城市', on_delete=models.CASCADE)
#     brand = models.ForeignKey(Brand, verbose_name='Brand 品牌', on_delete=models.CASCADE)
#     parcsqty = models.FloatField('10_sales', null=True, blank=True)
#     countryparcsqty = models.ForeignKey(countryparcs, on_delete=models.CASCADE)
#     countrystoreqty = models.ForeignKey(countryStoreQty, on_delete=models.CASCADE)
#     monthlysold = models.ForeignKey(cityMonthlySold, on_delete=models.CASCADE)
#     countrymonthlysold = models.ForeignKey(countryMonthlySold, on_delete=models.CASCADE)
#
#     def save(self, *args, **kwargs):
#         self.slug = slugify(self.parcsqty)
#         super(orphanTotals, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.city.city

    # total stores (FK month)
    # total stores - duplicates (FK month)
    # total stores