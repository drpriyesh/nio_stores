import requests
import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import re
import pandas as pd
import json
from pathfinder import pathfinder
from province_city_pop import pcpop
import time
import sys
import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
#
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
# path2 = sys.argv[1]

company = 'NIO'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def nio(path):

    url = "https://www.nio.cn/nio-places"
    context = ssl._create_unverified_context()
    headers = {}
    headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17'

    request = urllib.request.Request(url, headers=headers)
    html = urlopen(request, context=context)

    soup = BeautifulSoup(html, 'lxml')

    city_list = soup.find_all('div', {'class': 'nio-places--city'})
    city_urls = []
    for tag in city_list:
        aTags = tag.find_all("a", {"href": True})
        for tag in aTags:

            c = ('https://www.nio.cn' + tag["href"])
            print(c)
            city_urls.append(c)
    data = []
    for url in city_urls:

        request = urllib.request.Request(url, headers=headers)
        html = urlopen(request, context=context)
        soup = BeautifulSoup(html, 'lxml')
        place_list = soup.find_all('div', {'class': 'city-tile'})

        # print(place_list)
        # sort through the province id's to get 'pid' ('div', {'class': 'product'})


        for tag in place_list:

            datadict = {}
            city = tag.text.split('\n')[3][0:2]

            try:
                normal = pcpop(city)
            except TypeError:
                normal['city'] = city
                normal['province'] = city
                normal['tier'] = '其他'
            if not normal:
                normal['city'] = city
                normal['province'] = city
                normal['tier'] = '其他'

            c_norm = normal['city']
            p_norm = normal['province']
            t_norm = normal['tier']
            datadict['1_dealerID'] = ''
            datadict['2_name'] = tag.text.split('\n')[3] + ' ' + tag.text.split('\n')[2]
            type = tag.text.split('\n')[2]
            if ('蔚来中心' in type) or ('蔚来空间' in type):
                # print('type_orig: ', type)
                type = 'sales'
                # print('type_new: ', type)
            else:
                type = 'service'

            datadict['3_type_0'] = type
            datadict['3_type_1'] = tag.text.split('\n')[2]
            datadict['4_province'] = p_norm
            datadict['5_city'] = c_norm
            datadict['5_city_tier'] = t_norm
            datadict['6_address'] = tag.text.split('\n')[4]
            print(datadict['4_province'],datadict['5_city'],datadict['5_city_tier'],datadict['6_address'])
            data.append(datadict)



    service_url = "https://www.nio.cn/service-network"
    request = urllib.request.Request(service_url, headers=headers)
    html = urlopen(request, context=context)
    soup = BeautifulSoup(html, 'lxml')
    service_city_list = soup.find_all('div', {'class': 'nio-service--city'})

    for tag in service_city_list:

        # print(tag.text)
        # print(tag.text)
        print(tag.text.split('\n'))
        city= tag.text.split('\n')[1]
        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'

        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']
        divTags = tag.find_all("div", {"class": "nio-service--place"})
        # input("next......")

        for tag in divTags:
            datadict = {}
            print(tag.text)
            print(tag.text.split('\n'))
            # input("next......")
            datadict['1_dealerID'] = ''
            datadict['2_name'] = tag.text.split('\n')[1]

            datadict['3_type_0'] = 'service'
            datadict['3_type_1'] = tag.text.split('\n')[13]

            datadict['4_province'] = p_norm
            datadict['5_city'] = c_norm
            datadict['5_city_tier'] = t_norm
            datadict['6_address'] = tag.text.split('\n')[5]+tag.text.split('\n')[9]
            datadict['7_hours'] = tag.text.split('\n')[17]
            datadict['8_tel'] = tag.text.split('\n')[21]
            print(datadict)
            data.append(datadict)

    print(data)
    df = pd.DataFrame(data)
    print(len(df), df)
    df.drop_duplicates(keep=False, inplace=True)
    # df2.drop_duplicates(keep=False, inplace=True)
    print(len(df), df)


    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)


# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    nio(path)

