import requests
import json
import pandas as pd
import time
import os
import cookiejar
from province_city_pop import pcpop
from pathfinder import pathfinder, picklepath
company = 'Li_Auto'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def lixiang(path):
    # headers = {
    #     'authority': 'api-web.lixiang.com',
    #     'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    #     'x-chj-sourceurl': 'https://www.lixiang.com/?chjchannelcode=102002',
    #     'x-chj-traceid': 'f1f5b0e8900d842a6e75e3e8caf72157',
    #     'sec-ch-ua-mobile': '?0',
    #     'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36',
    #     'content-type': 'application/json',
    #     'accept': 'application/json, text/javascript, */*; q=0.01',
    #     'x-chj-metadata': '{"code":102003}',
    #     'origin': 'https://www.lixiang.com',
    #     'sec-fetch-site': 'same-site',
    #     'sec-fetch-mode': 'cors',
    #     'sec-fetch-dest': 'empty',
    #     'referer': 'https://www.lixiang.com/',
    #     'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
    #     'cookie': 'share-uid=; X-CHJ-SourceUrl=https%3A%2F%2Fwww.lixiang.com%2F%3Fchjchannelcode%3D102002; X-LX-Deviceid=ab5b0f67-f4d8-baa0-f744-17330a17dbad; gr_user_id=e49d7303-c3d4-45ae-b45a-dd0c9d5557e3; sajssdk_2015_cross_new_user=1; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%2217ae1c64fab144-0467d73bb0d1b8-34647600-1296000-17ae1c64fac43c%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_landing_page%22%3A%22https%3A%2F%2Fwww.lixiang.com%2F%22%7D%2C%22%24device_id%22%3A%2217ae1c64fab144-0467d73bb0d1b8-34647600-1296000-17ae1c64fac43c%22%7D; uid=5ea5fbe5-a165-41d7-8c9b-7df23943bd25; sid=7ffc9302-9c19-4676-b9df-8f3ff9ae22d8; JSESSIONID=eq43Z-1i4xoZm3Jel_IRWcL9XZSitC1gbV2-sVlO; a3c49546c2476d21_gr_session_id=e502b0e8-0d53-403b-99a1-518426ad9f9b; a3c49546c2476d21_gr_session_id_e502b0e8-0d53-403b-99a1-518426ad9f9b=true',
    # }
    headers = cookiejar.lixiang()['headers']
    url = cookiejar.lixiang()['url']
    params = (
        ('types', 'RETAIL,DELIVER,AFTERSALE,SPRAY,TEMPORARY_EXHIBITION,TEMPORARY_AFTERSALE_SUPPORT'),
        ('sortType', 'CITY'),
        ('storeEffectiveStatus', ''),
    )

    response = requests.get(url, headers=headers, params=params)

    data = []
    r2=[]

    for dealer in response.json()['data']:

        datadict = {}
        datadict2 = {}
        print(dealer)
        city = dealer['cityName']
        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'

        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']
        datadict['1_dealerID'] = dealer['id']
        datadict['2_name'] = dealer['name']
        type = dealer['type']
        if ('RETAIL'in str(type) or 'TEMPORARY_EXHIBITION'in str(type)):
            type = 'sales'
        else:
            type = 'service'


        datadict['3_type_0'] = type
        datadict['3_type_1'] = dealer['type']
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        datadict['6_address'] = dealer['address']
        try:
            datadict['7_hours'] = dealer['openingHours']
        except KeyError:
            datadict['7_hours'] = ''
        datadict['8_tel'] = dealer['telephone']

        if '店）' in dealer['name']:
            if '合作店' in dealer['name'] or '临时店' in dealer['name']:
                datadict['9_franch./self-own'] = 'self'
            else:
                datadict['9_franch./self-own'] = 'franch'
        elif '授权' in dealer['name']:
            datadict['9_franch./self-own'] = 'franch'
        else:
            datadict['9_franch./self-own'] = 'self'
        if 'status' in datadict:
            if datadict['status'] != "INCONSTRUCTION":
                r2.append(datadict)
        datadict2 = {**datadict, **dealer}

        data.append(datadict2)

        # datadict.clear()
        # datadict2.clear()

    df = pd.DataFrame(data)
    # dropping ALL duplicate values
    df2 = pd.DataFrame(r2)
    print(len(df), len(df2))

    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.drop_duplicates(keep=False, inplace=True)
    if 'status' in df:
        df[df["status"].str.contains("INCONSTRUCTION") == False]

    print(len(df), len(df2))

    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company, engine='xlsxwriter')
        df2.to_excel(writer, sheet_name=company + '_1', engine='xlsxwriter')
    print("Scraping complete: ", path)


# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    lixiang(path)
