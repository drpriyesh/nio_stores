import requests
import pandas as pd
import os
import time
import sys
import os
from province_city_pop import pcpop
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
from pathfinder import pathfinder, picklepath
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity

# path2 = sys.argv[1]

company = 'Toyota_FAW'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def toyotafaw(path):
    response = requests.get('https://www.ftms.com.cn/website/Dealer/getDealer')
    r = []
    r2 = []

    print(response.json()['data']['list'])
    for dealer in response.json()['data']['list']:

        datadict = {}
        datadict2 = {}
        city = dealer['city']
        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']



        datadict['1_dealerID'] = dealer['id']
        datadict['2_name'] = dealer['name']
        datadict['3_type_0'] = 'sales and service'
        datadict['3_type_1'] = ''
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        datadict['6_address'] = dealer['address']
        datadict['7_hours'] = ''
        datadict['8_tel_sales'] = dealer['phone_seal']
        datadict['8_tel_service'] = dealer['phone_service']
        r2.append(datadict)

        print(dealer)
        datadict2 = {**datadict, **dealer}
        r.append(datadict2)
        # for k, v in dealer.items():
        #     print(k, v)

    # df = pd.DataFrame(r)
    # df.to_excel(path, sheet_name=company)
    # print("Scraping complete: ", path)
    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.astype(str).drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company+'_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    toyotafaw(path)


# id 1383
# name 佛山中裕丰田
# fullname 佛山中裕丰田
# lng 0.000000
# lat 0.000000
# phone_seal 0757-85338866
# phone_service 0757-85338899
# address 广东省佛山市禅城区南庄镇醒群工业开发区自编门牌号16号
# province 广东
# provinceid 440000
# city 佛山
# cityid 440600
# tag []
# code 418AA
# distance 12223.64
#