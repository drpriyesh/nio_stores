import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.nio_project.settings')
# import django
# django.setup()
import cookiejar
import requests
import urllib
from urllib.request import urlopen
from bs4 import BeautifulSoup
import ssl
import pandas as pd

import time
import sys
from province_city_pop import pcpop
from pickle_dat import pickling, unpickling
from pathfinder import pathfinder, picklepath
# from nio_project.nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
# path2 = sys.argv[1]

company = 'Nissan_DF'
path = pathfinder(company)

# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

# pickle_path = os.path.join(OEM_DIR, 'pickle_files/')
pickle_path = picklepath()
if not os.path.exists(pickle_path):
    os.makedirs(pickle_path)

def nissandf(path):
    # cookies = {
    #     'HMF_CI': '82753aaacf829e585402857dedaf3b33fafc9e076e789a2cbe83c6cf6b70bd8e9f',
    #     '_smt_uid': '60ee6dc3.3903aa39',
    #     '_jc_uid': '60ee6dc3.3903aa39',
    #     '__clickidc': '162623840818578645',
    #     'cig.perm.1674701870': '{"userId":"a4f68aa2aad87d63aa12504525e0501a"}',
    #     'SC_ANALYTICS_GLOBAL_COOKIE': 'a123ab94385f417fa24c970afdf10789|True',
    #     'sc_ext_contact': 'a123ab94385f417fa24c970afdf10789|True',
    #     'NTKF_T2D_CLIENTID': 'guestAC22276C-D391-E052-4917-A35F8359EA21',
    #     '_ga_NY1GRZCSLF': 'GS1.1.1626238604.1.1.1626240372.0',
    #     '_ga': 'GA1.3.377467378.1626238414',
    #     'ASP.NET_SessionId': 'lrrurpntcsffedpxvxmgburb',
    #     'NO_PAGE_DURATION': '2021/7/18 16:03:32',
    #     'HMY_JC': 'f81ee6c809ea3a1d4f31c9d7276e80379e36736f895347762ed7f49ccc158c9edd,',
    #     'cig.sess.1674701870': '{"site_id":"1195943075","se":1626597216043,"vid":"6042cbd21b6609627b0299001a477cf7","sid":"436e1a4a75a817907b8fcb6c1269dff1","st":1626595415917,"l":"59d5d3f672c71be22d56d6412600f360","ready":true,"p":"t","pv":0}',
    #     '_gid': 'GA1.3.1769034262.1626595416',
    #     '_gat': '1',
    #     'no_screen': '1440%7C900',
    #     'sc_ext_session': '0hytkyr1teev0a33stopvuun',
    #     'Place': '%7B%22localProvince%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%2C%22province%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%2C%22city%22%3A%22%E4%B8%8A%E6%B5%B7%E5%B8%82%22%7D',
    # }
    #
    # headers = {
    #     'Connection': 'keep-alive',
    #     'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    #     'Accept': 'application/json, text/javascript, */*; q=0.01',
    #     'X-Requested-With': 'XMLHttpRequest',
    #     'sec-ch-ua-mobile': '?0',
    #     'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    #     'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    #     'Origin': 'https://www.dongfeng-nissan.com.cn',
    #     'Sec-Fetch-Site': 'same-origin',
    #     'Sec-Fetch-Mode': 'cors',
    #     'Sec-Fetch-Dest': 'empty',
    #     'Referer': 'https://www.dongfeng-nissan.com.cn/buy/find-dealer',
    #     'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    # }
    headers = cookiejar.nissanDF()['headers']
    cookies = cookiejar.nissanDF()['cookies']
    url1 = cookiejar.nissanDF()['url1']
    # provinces are numbered 1 to 31. Need for loop to get all cities in each 31 provinces

    city_ids = unpickling(pickle_path + company + 'city_ids')
    if not city_ids['data']:
        city_ids = []
    else:
        city_ids = city_ids['data']

    try:
        picklecount1 = unpickling(pickle_path + company + 'count')['data'] #.pop()
    except TypeError:
        picklecount1 = 0
    if not picklecount1:
        picklecount1 = 0



    count = 0
    print('city_ids:', city_ids)
    print('picklecount1: ', picklecount1)


    for province in range(1,32):
        if int(picklecount1) <= count:
            params = (('provinceId', str(province)),)
            get_city_ids = requests.get(url1, headers=headers, params=params)
            city_ids.append(get_city_ids.json()['data']['CityInfoList'])
            print(count, province, get_city_ids.json()['data']['CityInfoList'])
            pickling(city_ids, pickle_path + company + 'city_ids')
            pickling(count, pickle_path + company + 'count')
            count += 1
    print('city_ids: ',city_ids)

    count = 0
    count2 = 0
    change_p = 0

    r = unpickling(pickle_path + company + 'dealers')
    if not r['data']:
        r = []
    r2 = unpickling(pickle_path + company + 'dealers2')
    if not r2['data']:
        r2 = []
    try:
        picklecount2 = unpickling(pickle_path + company + 'dealercount')['data']  # .pop()
    except TypeError:
        picklecount2 = 0
    if not picklecount2:
        picklecount2 = 0
    print('picklecount2: ', picklecount2)
    for cities in city_ids:
        for city in cities:

            print(city)
            # for s in city:
                # print(s)
    # from get city ids need to insert each one to the 'city' part using for loop
            data = {
              'storeName': '',
              'province': '',
              'cprovince': '',
              'city': city['Name'],
              'cID': '',
              'carSeriesId': '',
              'terminal': '0'
            }

            try:
                normal = pcpop(city['Name'])
            except TypeError:
                normal['city'] = city['Name']
                normal['province'] = city['Name']
                normal['tier'] = '其他'
            if not normal:
                normal['city'] = city['Name']
                normal['province'] = city['Name']
                normal['tier'] = '其他'
            c_norm = normal['city']
            p_norm = normal['province']
            t_norm = normal['tier']
            url2 = cookiejar.nissanDF()['url2']
            response = requests.post(url2, headers=headers, cookies=cookies, data=data)
            # print(response.json()['data']['DealerInfos'])

            for dealer in response.json()['data']['DealerInfos']:

                datadict = {}
                datadict2 = {}
                if int(picklecount2) <= count2:
                    datadict['1_dealerID'] = dealer['StoreID']
                    datadict['2_name'] = dealer['StoreName']

                    if dealer['CertificationLabels']:
                        datadict['3_type_0'] = 'sales and service'
                        datadict['3_type_1'] = dealer['CertificationLabels']

                    else:
                        datadict['3_type_0'] = 'sales and service'
                        datadict['3_type_1'] = ''

                    datadict['4_province'] = p_norm
                    datadict['5_city'] = c_norm
                    datadict['5_city_tier'] = t_norm
                    datadict['6_address'] = dealer['Address']
                    datadict['7_hours'] = ''
                    datadict['8_tel_sales'] = dealer['SaleTel']
                    datadict['8_tel_service'] = dealer['ServiceTel']

                    r2.append(datadict)
                    pickling(r2, pickle_path + company + 'dealers2')

                    datadict2 = {**datadict, **dealer}
                    r.append(datadict2)
                    pickling(r, pickle_path + company + 'dealers')


                count2 += 1

    r = unpickling(pickle_path + company + 'dealers')['data']
    r2 = unpickling(pickle_path + company + 'dealers2')['data']

    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.astype(str).drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    nissandf(path)
# {'CertificationLabels': ['二手车置换', '售后服务', '延时服务'],
# 'Activities': [],
# 'StarLabels': ['交车当天及时交车(26)', '经销店的车辆陈列合理，便于看车(26)', '销售人员专业，车 '解释非常清楚耐心又细致(123)'],
# 'StoreID': '3588', 'ProvinceID': '4', 'CityID': '86', 'StoreName': '阳泉东风南方太行',
# 'StoreUrl': 'https://mall.dongfeng-nissan.com.cn/dealerl': 'https://mall.dongfeng-nissan.com.cn/dealer/3588/activity',
# 'CommentUrl': '', 'SaleTel': '0353-2116666', 'ServiceTel': '0353-2116588',
# 'Address': '郊区李荫路黄沙岩段交通集ngitude': '113.576146098630005',
# 'Latitude': '37.925330017333998', 'IsNIM': 0,
# 'StarCSIScore': '4.7', 'StarCSICommentCount': 246, 'StarSSIScore': '4.8', 'StarSSICommentCount': 26,
# 'District': '郊区', 'StarAvgScore': '90', 'WarnType': '2'}

