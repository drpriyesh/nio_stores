import requests
import json
import pandas as pd
from province_city_pop import pcpop
import time
import sys
import os
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
#
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
from pathfinder import pathfinder, picklepath
# path2 = sys.argv[1]

company = 'VW_FAW'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')

def vwfaw(path):

    response = requests.get('https://contact.faw-vw.com/uploadfiles/js/dealer.js')


    print(response.text[16:])
    a = response.text[16:]
    b = json.loads(a)
    r = []
    r2 = []

    count = 0
    for dealer in b:

        datadict = {}
        datadict2 = {}
        city = dealer['vc_name']

        # count += 1
        # print(count, dealer['Address'])

        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']



        datadict['1_dealerID'] = str(dealer['vd_id']) + '   ' + str(dealer['vd_dealerCode'])
        datadict['2_name'] = dealer['vd_dealerName']
        datadict['3_type_0'] = 'sales and service'
        datadict['3_type_1'] = dealer['vd_dealerType']
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        if not dealer['vd_postCode']:

            datadict['6_address'] = dealer['vd_address']
        else:

            datadict['6_address'] = dealer['vd_address'] + '  ' + str(dealer['vd_postCode'])
        datadict['7_hours'] = ''
        datadict['8_tel'] = dealer['vd_salePhone']
        r2.append(datadict)
        # datadict = dealer
        datadict2 = {**datadict, **dealer}
        r.append(datadict2)

    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.drop_duplicates(keep=False, inplace=True)
    df2.drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)

# Start execution here!
if __name__=='__main__':

    print('Starting script...')
    vwfaw(path)

    # for k, v in dealer.items():
    # {'vd_salePhone': '0551-63520168', 'vd_serviceCode': None, 'vd_dealerName': '安徽孚迪汽车销售有限公司',
    #  'vd_longitude': '117.353708', 'vd_address': '包河区纬一路13号专卖店',
    #  'v1', 'vc_name': '合肥', 'vd_url': None, 'vd_dealerCode': 'S734009', 'vd_dealerType': 1, 'vd_city': 113, 'vd_province': 16, 'vd_postCode': '230051', 'vd_fax': None, 'vp_name': '安徽', 'vd_id': 103}
