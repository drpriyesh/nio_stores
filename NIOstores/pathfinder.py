import time
import sys
import os


def pathfinder(company):

    datestr = time.strftime("%Y%m%d")
    timestr = time.strftime("%Y%m%d_%H%M%S")
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
    if not os.path.exists(OEM_DIR):
        os.makedirs(OEM_DIR)
    print(OEM_DIR)
    path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')
    return path

def picklepath():
    datestr = time.strftime("%Y%m%d")
    timestr = time.strftime("%Y%m%d_%H%M%S")
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
    pickle_path = os.path.join(OEM_DIR, 'pickle_files/')
    return pickle_path