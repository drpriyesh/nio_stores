


# curl 'https://mall.svw-volkswagen.com/cop-uaa-adapter/api/v1/dealers?&regionCode=310100&sort=level&current=1&size=10&keyWord='
import requests
import pandas as pd
from province_city_pop import pcpop
import time
import sys
import os
from pathfinder import pathfinder, picklepath
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nio_project.settings')
# import django
# django.setup()
#
# from nio_dealer.models import Province, City, Company, Dealer, Salesbyprovince, Salesbycity
#
#
# path2 = sys.argv[1]

company = 'VW_SAIC'
path = pathfinder(company)
# datestr = time.strftime("%Y%m%d")
# timestr = time.strftime("%Y%m%d_%H%M%S")
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# OEM_DIR = os.path.join(BASE_DIR, datestr + '_Nio')
# if not os.path.exists(OEM_DIR):
#     os.makedirs(OEM_DIR)
# print(OEM_DIR)
# path = (OEM_DIR + '/' + timestr + '_' + company + '_exported_json_data.xlsx')
# # first to get city code or "region" codes need to parse:
# city_response = requests.get('https://mall.svw-volkswagen.com/cop-uaa-adapter/api/v1/city/trees;')
# print(city_response.json()['data']['children'])
#
# for dealer in city_response.json()['data']['children']:
#     print('\n')
#     count = 0
#     for k, v in dealer.items():
#         if count != 0:
#             print(k, v)
#             # input("Press Enter to continue...")
#         count += 1
# # neeed to assign region by code from city response - use for loop

def vwsaic(path):
    params = (
        ('regionCode', ''),
        ('sort', 'level'),
        ('current', '1'),
        ('size', '100000'),
        ('keyWord', ''),
    )



    response = requests.get('https://mall.svw-volkswagen.com/cop-uaa-adapter/api/v1/dealers', params=params)
    # need convert address to utf-8
    print(response.json()['data'])
    count = 0
    r = []
    r2 = []

    for dealer in response.json()['data']['records']:

        datadict = {}
        datadict2 = {}
        city = dealer['city']

        # count += 1
        # print(count, dealer['Address'])

        try:
            normal = pcpop(city)
        except TypeError:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        if not normal:
            normal['city'] = city
            normal['province'] = city
            normal['tier'] = '其他'
        c_norm = normal['city']
        p_norm = normal['province']
        t_norm = normal['tier']



        datadict['1_dealerID'] = str(dealer['orgId']) + '   ' + str(dealer['orgCode']) +  '   ' + str(dealer['entityCode'])
        datadict['2_name'] = dealer['orgName']
        datadict['3_type_0'] = 'sales and service'
        datadict['3_type_1'] = str(dealer['orgType']) + '   ' + str(dealer['orgTypeName']) +  '   ' + str(dealer['featureTags'])
        datadict['4_province'] = p_norm
        datadict['5_city'] = c_norm
        datadict['5_city_tier'] = t_norm
        if not dealer['cityCode']:
            datadict['6_address'] = dealer['address']
        else:
            datadict['6_address'] = dealer['address'] + '   ' + str(dealer['cityCode'])
        datadict['7_hours'] = dealer['businessHours']
        datadict['8_tel_sales'] = dealer['salesPhone']
        datadict['8_tel_service'] = dealer['servicePhone']

        r2.append(datadict)
        datadict2 = {**datadict, **dealer}
        r.append(datadict2)

    df = pd.DataFrame(r)
    df2 = pd.DataFrame(r2)
    df.astype(str).drop_duplicates(keep=False, inplace=True)
    df2.astype(str).drop_duplicates(keep=False, inplace=True)
    with pd.ExcelWriter(path) as writer:
        df.to_excel(writer, sheet_name=company)
        df2.to_excel(writer, sheet_name=company + '_1')
    print("Scraping complete: ", path)


# Start execution here!
if __name__ == '__main__':
    print('Starting script...')
    vwsaic(path)
    # for k, v in dealer.items():
    #     print(k, v)
    # {'address': '湖南省醴陵市汽配产业园（320国道南）', 'afterSalesStatus': None, 'banks': [], 'businessHours': '9:00-18:00',
    #  'category': None, 'characteristic': [], 'city': ': '430200', 'contacts': [], 'createdAt': '1517821674000', '
    #  description': None, 'distance': None, 'enName': 'Li Ling Zhong Xin Da', 'entityCode': 'VW422059', 'featureTags': ['
    #  新能源', '自营', '市场活动', '售后业务全功能', '售后预约资源功能', '线上市场活动', '智慧网销', 'MEB快捷签', 'ICE快捷签'], 'imagesApp': [], '
    #  imagesPc': [], 'lbsLatitude': '27.709el': None, 'orgCode': '3410202', 'orgId': '397508008681382176', 'orgName
    #  ': '醴陵中鑫达汽车销售有限公司', 'orgType': 'F', 'orgTypeName': '4S店', 'province': '湖南省', '
    #  province'region': '醴陵市', 'regionCode': '430281', 'salesPhone': '0731-23339108', 'salesStatus': None, 'score': None, 'serviceCode': '74348060', 'servicePhone': '0731-23339109', 'sta': 1, 'telephone': None, 'thumbApp': None, 'thumbPc': None, 'updatedAt': '1624515303000'}
